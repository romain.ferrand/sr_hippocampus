\documentclass[info]{ensrennesbeamer}
%\documentclass{beamer}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{pgfplots} 
\title{Hyperparameters}
\subtitle{and hyperparameter-less gradient descent}


\author{Romain Ferrand}
\institute{Investigation of the successor representation for neuro-computational models of the hippocampus}
\date{September 5, 2018}
\newcommand\alertb[2]{\begin{alertblock}{#1}#2\end{alertblock}}
\newcommand\exampleb[2]{\begin{exampleblock}{#1}#2\end{exampleblock}}
\newcommand\newb[2]{\begin{block}{#1}#2\end{block}}
\newcommand {\framedgraphic}[2] {
	\begin{frame}{#1}
		\begin{center}
			\includegraphics[width=2.\textwidth,height=1.3\textheight,keepaspectratio]{#2}
		\end{center}
	\end{frame}
}

\begin{document}
	
	\begin{frame}
		\titlepage
	\end{frame}
	
	\begin{frame}{Sommaire}
		\tableofcontents
	\end{frame}
	
	\section{Hyperparameters}
	\subsection{Context}
	\begin{frame}
		\newb{Context}{
			\begin{itemize}
				\item Machine learning model
				\item Supervised / Unsupervised learning
				\item lots of data
			\end{itemize}
		}
	\end{frame}
	\begin{frame}
		\newb{Supervised learning}{
			"task of learning a function that maps an input to an output based on example input-output pairs."\footnotemark
			\footnotetext{Stuart J. Russell, Peter Norvig (2010) Artificial Intelligence: A Modern Approach, Third Edition, Prentice Hall ISBN 9780136042594.}
			\begin{itemize}
				\item partial information on raw data (training set)
				\item The output space in known
			\end{itemize}
		}
		\exampleb{Regression}{
			Predict house's prices (output) thanks to 
			house's features (input)
		}
		\exampleb{Classification}{
			Predict if a mail is a spam or not (output)
			thanks to a words count (input)
		}
	\end{frame}
	\begin{frame}
		\newb{Unsupervised learning}{
			Find structures in the raw data
		}
		\exampleb{Clustering}{
			Divide the raw data in homogeneous subgroups}
		\exampleb{Association}{
			Find correlation between items in the raw data}
	\end{frame}
	\subsection{What is an Hyperparameter?}
	\begin{frame}
		\newb{Hyperparameters}{
			A parameter which its value is set before the learning process
		}
		\newb{Parameters/Hyperparameters}{
			Parameters:
			\begin{itemize}
				\item The weights of a neural network
				\item Tree's structure inside a random forest
				\item The orthogonal vector of the hyperplan in a SVM 
			\end{itemize}
			Hyperparameters:
			\begin{itemize}
				\item The layer's structure of a neural network
				\item The number of tree in a random forest
				\item The scoring function of the ML model
			\end{itemize}
		}
		\newb{Observations}{
			Types of hyperparameters:
			\begin{itemize}
				\item Models choices: task based
				\item Structural choices: number of neurons, optimization method
				\item Algorithmic choices: learning rate, dropout probability
			\end{itemize}
			Hierarchical dependencies:
			\begin{itemize}
				\item Model $\rightarrow$ Structure $\rightarrow$ Algorithms
			\end{itemize}
		}
	\end{frame}
	\subsection{Why do we want to get rid of them?}
	\begin{frame}
		\newb{Why do we want to get rid of them?}{
			For the scientific community:
			\begin{itemize}
				\item Reproductive science
				\item Significance of results
				\item Machine learning algorithms as Toolbox for non-computer scientists.
			\end{itemize}
			For Mnemosyne team
			\begin{itemize}
				\item Being more coherent with their 
				algorithmic analogies of the cognition functions
			\end{itemize}
		}
	\end{frame}
	\subsection{How to get rid of them?}
	\begin{frame}
		Its not so clear...
	\end{frame}
	\begin{frame}
		It's not so clear...but we have some ideas!
	\end{frame}
	\begin{frame}
		\newb{General guidelines...}{
			\begin{itemize}
				\item Choose the simplest model possible in function of the task
				\item Add prior information as much as possible
				\item Find bounds and relevant precision 
				\item Find if the hyperparameter value is relevant
			\end{itemize}
		}
		\newb{...and a general solution}{
			Automatic learning:
			\begin{itemize}
				\item Try everything choose the best models
				\item Surrogate the score distribution in function of numerical hyperparameters
			\end{itemize}
		}
	\end{frame}
	\framedgraphic{Architecture}{nn_tree.pdf}
	\begin{frame}
		\newb{Results ranking system:\footnotemark}{
			\begin{description}
				\item[Generalization] Average score of the model on the test dataset
				\item[Speed] Average runtime on the test dataset
				\item[Stability] Standard deviation of the score
				\item[Simplicity] Complexity of the model: number of hyperparameters, dimensionality
				\item[Interpretability] How easily the model can be understood by a human
				\footnotetext{Bermúdez-Chacón et al 2015}
			\end{description}
		}
	\end{frame}
	\section{Hyperparameter-less gradient descent}
	\subsection{ADADELTA gradient descent}
	\begin{frame}
		\newb{Update rule}{
			\begin{align*}
			E[g^{2}]_{t} &= \gamma E[g^{2}]_{t-1}+(1-\gamma)g_{t}^{2}\\
			E[\Delta \omega^{2}]_{t} &= \gamma E[\Delta \omega^{2}]_{t-1}+
			(1-\gamma)\Delta \omega^{2}\\
			E[g^{2}]_{0} &= 0 \quad E[\Delta \omega^{2}]_{0} = 0 \quad \gamma \in ]0,1[\\
			\Delta \omega_{t} &= - \frac{\sqrt{E[\Delta \omega^{2}]_{t-1} + \epsilon}}
			{\sqrt{E[g^{2}]_{t} + \epsilon}}g_{t}
			\end{align*}
		}
		\
	\end{frame}
	\subsection{Hyper-parameters}
	\begin{frame}
		\newb{$\gamma$ the window parameter}{
			\begin{itemize}
				\item express the importance of past result on the learning process
				\item generally set close to 1
			\end{itemize}
		}
		\newb{$\epsilon$ the epsilon parameter}{
			\begin{itemize}
				\item set to a small positive value 
				(ie: $10^{-n}$ where $n \in [0, 16]\cap \mathbb{N}$)
				\item prevent numerical behaviors 
				\item start the Adadelta learning
			\end{itemize}
		}
		\subsection{Statistical influences}
		
	\end{frame}
\end{document}