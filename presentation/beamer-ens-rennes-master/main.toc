\babel@toc {english}{}
\beamer@sectionintoc {1}{The hippocampus}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Cognitive context}{4}{0}{1}
\beamer@subsectionintoc {1}{2}{The cognitive map theory}{7}{0}{1}
\beamer@subsectionintoc {1}{3}{The predictive map theory}{8}{0}{1}
\beamer@sectionintoc {2}{Neurobiologically compatible Reinforcement learning}{9}{0}{2}
\beamer@subsectionintoc {2}{1}{General definition of RL}{10}{0}{2}
\beamer@subsectionintoc {2}{2}{Actor-critic learning}{12}{0}{2}
\beamer@subsectionintoc {2}{3}{The successor representation}{14}{0}{2}
\beamer@subsectionintoc {2}{4}{Multi-time scale SR}{16}{0}{2}
\beamer@sectionintoc {3}{Contribution}{19}{0}{3}
\beamer@subsectionintoc {3}{1}{Learning with distance information}{20}{0}{3}
\beamer@subsectionintoc {3}{2}{Food retrieval modelling}{21}{0}{3}
\beamer@subsectionintoc {3}{3}{Goal-directed behaviour modelling}{26}{0}{3}
\beamer@subsectionintoc {3}{4}{Clustering for sub-goal discovery}{29}{0}{3}
\beamer@sectionintoc {4}{Discussion}{34}{0}{4}
