from maze_actor_critic import MazeActorCritic
import numpy as np
import utils
import sys
import numpy
import findiff
from scipy.special import factorial
from scipy.stats import rankdata
import time
numpy.set_printoptions(threshold=sys.maxsize)


class MazeActorCriticSRTDDistance(MazeActorCritic):

    def __init__(self, maze_file):
        super().__init__(maze_file)
        # option for without AC comparaison
        # 0.0 means no epsi-greedy > 0.0 means with epsi-greedy
        self.epsilon = 0.0
        # minimal time bin
        self.t_star_min = 0.0
        # space btw bin
        self.c = 0.0
        # precision of post approximation
        self.k = 0
        # number of sr windows
        self.nb_windows = 0
        # bins
        self.t_star = np.array([], float)
        # sigma association
        self.sigma = np.array([], float)
        # gamma discount factor
        self.discount_sr = np.array([], float)
        # step taken to the maximal possible reward for the run
        # the value is None if the reward taken is not the best possible
        self.max_reward = 0
        self.nu = 0.0
        self.coeff_dist = 0
        self.trace_features_sr = np.array([], float)
        # self.trace_features_sr = np.zeros((self.nb_cells,))

        self.w_ij = np.zeros((self.nb_action, self.nb_cells,))
        self.w_ij_sr = np.array([], float)
        self.distance = np.empty((self.nb_cells, self.nb_cells,), float)
        self.alpha_sr = 0.0
        self.lambda_sr = 0.0
        self.mu = 0.0
        self.w_j = np.zeros((self.nb_cells,))
        self.previous_features = np.zeros((self.nb_cells,))
        self.features = np.zeros((self.nb_cells,))
        self.current_state = self.initial_state
        self.prev_action = np.zeros((self.nb_action,))
        self.signal = 0.0

        self.state = 0.0
        self.state_prime = 0.0

        self.sr_state = np.zeros((self.nb_windows, self.nb_cells,))
        self.sr_state_prime = np.zeros((self.nb_windows, self.nb_cells,))

        self.delta_d = np.zeros((self.nb_cells,))

        self.state_dist = np.zeros((self.nb_cells,))
        self.state_dist_prime = np.zeros((self.nb_cells,))

        self.animate_flag = False

    def set_params(self, params):
        # minimal time bin
        self.t_star_min = params["t_star_min"]
        # space btw bin
        self.c = params["c"]
        # precision of post approximation
        self.k = params["k"]
        # number of sr windows
        self.nb_windows = params["nb_windows"]
        self.nu = params["nu"]
        self.mu = params["mu"]
        self.alpha_sr = params["alpha_sr"]
        self.lambda_sr = params["lambda_sr"]
        self.discount = params["discount"]
        self.t_star = np.array([self.t_star_min * (1 + self.c) ** i for i in range(self.nb_windows)], float)
        self.sigma = self.k / self.t_star
        self.discount_sr = np.exp(-self.sigma)
        self.w_ij_sr = np.tile(np.identity(self.nb_cells, float), (self.nb_windows, 1)).reshape((
            self.nb_windows, self.nb_cells, self.nb_cells))
        self.epsilon = params["epsilon"]

    def animate(self):
        if self.animate_flag:
            _str = ""
            for i in range(self.height):
                for k in range(2):
                    if k == 0:
                        for j in range(self.width):
                            if self.tr_matrix[self.ij_to_number(i, j), 0] == self.ij_to_number(i, j):
                                _str += "+---"
                            else:
                                _str += "+   "
                        _str += "\n"
                    else:
                        for j in range(self.width):
                            if self.tr_matrix[self.ij_to_number(i, j), 2] == self.ij_to_number(i, j):
                                _str += "|"
                            else:
                                _str += " "
                            if self.current_state == self.ij_to_number(i, j):
                                _str += " o "
                            else:
                                _str += "   "
                        _str += "\n"
            for j in range(self.width):
                if self.tr_matrix[self.ij_to_number(self.height - 1, j), 3] == self.ij_to_number(self.height - 1, j):
                    _str += "+---"
                else:
                    _str += "+   "
            _str += "\n"
            time.sleep(0.1)
            sys.stdout.write(_str)
            sys.stdout.flush()
            print("state = {}".format(self.state))
            print("state_prime = {}".format(self.state_prime))
            print('norm w_j = {}'.format(np.linalg.norm(self.w_j)))
            print(f'signal {self.signal}')
            print(f"reward = {self.current_reward}")
            print(utils.print_table("State distance", self.state_dist, self.width))
            print(utils.print_table("State distance prime", self.state_dist_prime, self.width))
            print(utils.print_table("Previous features", self.previous_features, self.width))
            print(utils.print_table("Features", self.features, self.width))
            print(utils.print_table("W_j", self.w_j, self.width))

    def new_sr_state(self, features):
        return np.dot(np.transpose(self.w_ij_sr, (0, 2, 1)), features)

    def update(self, sample_maze):
        self.update_sr()
        if not sample_maze:
            self.update_actor()
            self.update_critic()
            self.compute_distance()

    def new_distance(self, features):
        dist = np.dot(self.distance.transpose(), features)
        dist_log = self.k / dist
        dist_exp = np.exp(-dist_log)
        ind = np.argpartition(dist_log, -2)[-2:]
        ind = ind[np.argsort(dist_log[ind])]
        # dist_log = gaussian_filter1d(dist_log, 1)
        # dist_log = np.where(dist == self.t_star[-1] + 1, 0.0, dist_log)
        dist_exp = 1 / rankdata(dist_exp, "dense")

        dist_exp = np.where(dist == -1, 0.0, dist_exp)
        dist_log = np.where(dist == -1, 0.0, dist_log)
        dist_log[ind[1]] = dist_log[ind[0]] * 1.5
        return dist_exp

    # def update_sr(self, features, sr_state, sr_state_prime):
    #    delta = features + self.discount_sr[..., np.newaxis] * sr_state_prime - sr_state
    #    for i in range(self.nb_windows):
    #        self.w_ij_sr[i] += self.alpha_sr * features[..., np.newaxis] * delta[i]

    def update_sr(self):
        for i in range(self.nb_windows):
            update_sr_wij = self.previous_features + self.discount_sr[i] * self.sr_state_prime[i] - self.sr_state[i]
            self.w_ij_sr[i] += self.alpha_sr * self.previous_features[..., np.newaxis] * update_sr_wij

    def update_actor(self):
        update = self.nu * self.signal * self.previous_features
        self.w_ij[self.action] += update

    def update_critic(self):
        norm = np.dot(self.state_dist, self.state_dist)
        self.w_j += self.nu * self.signal * self.state_dist / norm

    def v_value(self, state_dist):

        return np.dot(state_dist, self.w_j)

    def reset_maze(self):
        super(MazeActorCriticSRTDDistance, self).reset_maze()
        self.state = np.zeros((self.nb_windows,))
        self.state_prime = np.zeros((self.nb_windows,))
        self.features = np.zeros((self.nb_cells,))
        self.previous_features = np.zeros((self.nb_cells,))
        self.sr_state = np.zeros((self.nb_windows, self.nb_cells,))
        self.sr_state_prime = np.zeros((self.nb_windows, self.nb_cells,))
        self.signal = 0.0

    def compute_distance(self):
        const_inv_laplace_coef = (-1) ** self.k / factorial(self.k) * self.sigma ** (self.k + 1)
        d_dx = findiff.FinDiff(0, self.k)
        for i in range(self.nb_cells):
            for j in range(self.nb_cells):
                total_w_ij_sr = np.sum(self.w_ij_sr[:, i, j])
                # check if the cell is accessible
                if total_w_ij_sr == 0.0 or total_w_ij_sr == 1.0:
                    self.distance[i, j] = -1
                elif i == j:
                    self.distance[i, j] = self.t_star_min - sys.float_info.epsilon
                else:
                    pair_dist = const_inv_laplace_coef * d_dx(self.w_ij_sr[:, i, j])
                    max_pic = np.argmax(pair_dist)
                    self.distance[i, j] = self.t_star[max_pic]

    def new_action(self, q_values, sample_maze):
        if self.epsilon == 0.0:
            if sample_maze:
                return self.new_possible_random_action()
            else:
                return self.new_possible_action(q_values)
        else:
            # this operation only means something for one-hot coding state
            # I'll build a proper class for this version after my internship is finish
            q_values = self.w_j[self.tr_matrix[self.current_state, : -1]]
            q_values = np.append(q_values, 0.0)
            action_prob = np.zeros_like(q_values)
            q_max = np.max(q_values)
            ind_q_max = np.argwhere(q_values == q_max).flatten()
            nb_max = len(ind_q_max)
            action_prob[ind_q_max] = (1 - self.epsilon) / nb_max
            action_prob += self.epsilon / len(q_values)
            return np.random.choice(range(self.nb_action), 1, p=action_prob)[0]

    def run(self, step, output, sample_maze, results_handler, animation):
        self.animate_flag = True if animation and not sample_maze else False
        self.animate()
        i = 1
        # step = -1 imply end on reward condition so step is unbounded
        if step == -1:
            step = float('inf')
        self.set_features(self.current_state)

        if not sample_maze:
            self.compute_distance()
        # handle the first (S,A,S') observation

        # observe state S
        self.sr_state = self.new_sr_state(self.features)
        if not sample_maze:
            self.state_dist = self.new_distance(self.features)
            self.state = self.v_value(self.state_dist)

        # new action
        if step == 1:
            reward = np.dot(self.features, self.rewards)
            if reward > 0:
                self.action = self.nb_action - 1
                self.current_reward = reward
            else:
                q_values = self.eval(self.features)
                self.action = self.new_action(q_values, sample_maze)
                self.features = self.new_features(self.current_state, self.action)

                if not sample_maze:
                    self.current_reward = self.new_reward(self.features, sample_maze)

        # observe state S'
        self.sr_state_prime = self.new_sr_state(self.features)
        if not sample_maze:
            self.state_dist_prime = self.new_distance(self.features)
            self.state_prime = self.v_value(self.state_dist_prime)
            self.signal = self.new_signal(self.state, self.state_prime)

        # update
        self.update(sample_maze)

        self.animate()

        if output:
            results_handler.parse_results_between_cycle(self)
        while i < step and self.current_reward <= 0.0 and (i <= 1000 or sample_maze):
            i += 1
            print(f"run = {i}")
            self.sr_state = self.sr_state_prime
            if not sample_maze:
                self.state_dist = self.state_dist_prime
                self.state = self.state_prime

            # new action
            q_values = self.eval(self.features)
            self.action = self.new_action(q_values, sample_maze)
            self.features = self.new_features(self.current_state, self.action)
            self.current_reward = self.new_reward(self.features, sample_maze)

            # observe state S'
            self.sr_state_prime = self.new_sr_state(self.features)

            if not sample_maze:
                self.state_dist_prime = self.new_distance(self.features)
                self.state_prime = self.v_value(self.state_dist_prime)
                self.signal = self.new_signal(self.state, self.state_prime)

            # update
            self.update(sample_maze)
            self.animate()

        if output:
            results_handler.parse_results_between_cycle(self)
        self.max_step = i
        if not sample_maze:
            self.max_reward = self.current_reward if self.current_reward == np.max(self.rewards) else None
        print(f"max_step = {self.max_step}")
        print(f"max_reward = {self.max_reward}")
        return i
