from maze_base import MazeBase
import numpy as np
import utils


class MazeActorCritic(MazeBase):

    def __init__(self, maze_file):
        super().__init__(maze_file)
        self.nu = 0.0
        self.signal = 0.0
        self.ld = 0.0
        self.w_ij = np.zeros((self.nb_action, self.nb_cells))
        self.w_j = np.zeros((self.nb_cells,))
        self.features = np.zeros((self.nb_cells,))
        self.previous_features = np.zeros((self.nb_cells,))
        self.trace_features = np.zeros((self.nb_cells,))
        self.action_prob = None

    def set_features(self, state):
        self.features[state] = 1

    def new_features(self, state, action):
        state_p = self.tr_matrix[state, action]
        self.previous_features = self.features.copy()
        features = np.zeros((self.nb_cells,))
        features[state_p] = 1
        self.current_state = state_p
        return features

    def new_reward(self, features, sample_maze):
        if sample_maze:
            return 0.0
        else:
            reward = np.dot(self.rewards, features)
            if reward > 0.0:
                return reward
            else:
                return -1.0

    def eval(self, features):
        return self.new_q(features)

    def new_signal(self, state, state_prime):
        # episodic learning
        if self.current_reward > 0.0:
            state_prime = 0.0
        return self.current_reward + self.discount * state_prime - state

    def set_params(self, params):
        self.nu = params["nu"]
        self.discount = params["discount"]

    def new_action(self, q_values, sample_maze):
        if sample_maze:
            return self.new_possible_random_action()
        else:
            return self.new_possible_action(q_values)

    """
    compute new action with wall consideration
    
    """

    def new_possible_action(self, q_values):
        action_prob = np.array(utils.softmax(q_values))
        impossible_action = np.array([i for i, x in enumerate(self.tr_matrix[self.current_state, :])
                                      if x == self.current_state])
        if impossible_action.size != 0:
            action_prob[impossible_action] = 0.0
        action_prob /= np.sum(action_prob)
        return np.random.choice(range(self.nb_action), 1, p=action_prob)[0]

    def new_random_action(self):
        act = np.random.choice(range(self.nb_action), 1)[0]
        return act

    def new_possible_random_action(self):
        possible_action = [i for i, x in enumerate(self.tr_matrix[self.current_state, :])
                           if x != self.current_state]
        # possible_action.append(self.nb_action - 1)
        # print(possible_action)
        act = np.random.choice(possible_action, 1)[0]

        return act

    def update_actor(self):
        self.w_ij[self.action] += self.nu * self.signal * self.previous_features

    def update_critic(self):
        self.trace_features = self.discount * self.ld * \
                              self.trace_features + self.previous_features
        self.w_j += self.nu * self.signal * self.trace_features

    def new_q(self, features):
        return np.dot(self.w_ij, features)

    def new_feature_value(self, features):
        return np.dot(self.w_j, features)

    def update(self, sample_maze):
        if sample_maze:
            return
        self.update_actor()
        self.update_critic()

    def run(self, step, output, sample_maze, results_handler, animation):
        if animation:
            self.animate()
        i = 1
        if step == -1:
            step = float('inf')
        self.set_features(self.current_state)

        self.state = self.new_feature_value(self.features)

        # new action
        if step == 1:
            reward = np.dot(self.features, self.rewards)
            if reward > 0:
                self.action = self.nb_action - 1
                self.current_reward = reward
            else:
                q_values = self.eval(self.features)
                self.action = self.new_action(q_values, sample_maze)
                self.features = self.new_features(self.current_state, self.action)
                if not sample_maze:
                    self.current_reward = self.new_reward(self.features, sample_maze)

        self.state_prime = self.new_feature_value(self.features)

        self.signal = self.new_signal(self.state, self.state_prime)
        self.update(sample_maze)
        if output:
            results_handler.parse_results_between_cycle(self)

        while i < step and self.current_reward <= 0.0:
            i += 1
            self.state = self.state_prime
            q_values = self.eval(self.features)

            self.action = self.new_action(q_values, sample_maze)
            self.features = self.new_features(self.current_state, self.action)

            self.current_reward = self.new_reward(self.features, sample_maze)
            if self.current_reward > 0.0:
                reward_temp = self.current_reward
                self.current_reward = -1.0
                self.state_prime = self.new_feature_value(self.features)
                self.signal = self.new_signal(self.state, self.state_prime)
                self.update(sample_maze)
                self.state = self.state_prime
                self.action = self.nb_action - 1
                self.current_reward = reward_temp

            self.state_prime = self.new_feature_value(self.features)
            self.signal = self.new_signal(self.state, self.state_prime)
            self.update(sample_maze)

            if animation:
                self.animate()
            if output:
                results_handler.parse_results_between_cycle(self)
        self.max_step = i
        return i
