import numpy as np
from ple.games.snake import Snake
from ple import PLE
class ActorCriticSnake:
    def __init__(self, decay:float, sr_lr:float, alpha_sr, lambda_sr):


if __name__ == '__main__':
    game = Snake()
    p = PLE(game, fps=30, display_screen=True)
    agent = ActorCriticSnake(allowed_actions=p.getActionSet())

    p.init()
    reward = 0.0

    for i in range(nb_frames):
        if p.game_over():
            p.reset_game()

        observation = p.getScreenRGB()
        action = agent.pickAction(reward, observation)
        reward = p.act(action)
