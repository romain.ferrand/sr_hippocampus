from pymongo import MongoClient
from pymongo.errors import ConnectionFailure, ConfigurationError
import numpy as np
import matplotlib.pyplot as plt
from scipy import interpolate
import sys

# first derivative with 5-point stencil
# _filter = [1, -8, 0, 8, -1]
# second derivative with 5-point stencil
# _filter = [-1, 16, -30, 16, -1]
# 4 deriv
# _filter = [1, -4, 6, -4, 1]
_filter = [7 / 240, -2 / 5, 169 / 60, -122 / 15, 91 / 8, -122 / 15, 169 / 60, -2 / 5, 7 / 240]


def mongo_connect():
    db = None
    try:
        client = MongoClient("mongodb://Romain:INExTIcTiNFESIBlEXalOturg@localhost:27017/sr_result?authSource=admin")
        client.admin.command('ismaster')
        db = client.sr_results
        print("Local connection establish")
    except (ConfigurationError, ConnectionFailure):
        print("Local server not available")

    return db


def print_table(title: str, table: np.ndarray, width: int):
    _str = title.center(40) + "\n"
    for k in range(0, len(table), width):
        _str += "\t".join(f"{val:3f}" for val in table[k:k + width]) + "\n"
    return _str.expandtabs()


def softmax(x):
    s = np.max(x)
    e_x = np.exp(x - s)
    div = np.sum(e_x)
    return e_x / div


def softmax_deriv(x):
    y = softmax(x)
    sm = y.reshape((-1, 1))
    res = np.diag(x) - np.dot(sm, sm.T)
    return res


def find_zero(a):
    up = np.insert(a, [0], 0)
    down = np.append(a, 0)

    a_mult = up * down
    a_amp = (up - down) ** 2
    ret_i = 0
    y, y_prime = (a[0], a[0])
    amp = 0
    for i, elem in enumerate(a_mult):
        if elem < 0.0 and a_amp[i] > amp:
            y, y_prime = (a[i - 1], a[i])
            amp = a_amp[i]
            ret_i = i

    return y, y_prime, ret_i


def distance_by_rows(row, dx):
    if np.sum(row) == 0.0:
        return 1
    # b-spline interpolation
    # row /= norm
    # print("row = {}".format(row))
    row /= np.dot(row, row)
    # interp = interpolate.splrep(dx, row, k=2)
    # row_transf = interpolate.splev(dx, interp, der=2)

    # row_transf = np.gradient(np.gradient(row, dx), dx)
    row_transf = np.convolve(row, _filter, mode="same")
    # print("row transf = {}".format(row_transf))
    y1, y2, i = find_zero(row_transf)
    if y1 == y2:
        sigma = 1
    else:
        t = - y1 / (y2 - y1)
        sigma = t * (dx[i] - dx[i - 1]) + dx[i - 1]
    # print("row = {}".format(row))
    # print("row_transf = {}".format(row_transf))

    return sigma


def distance_matrix(mx, dx):
    # print(mx.shape)
    z, y, x = mx.shape
    nb_cell = y
    dist = np.empty((y, x))
    for i in range(y):
        for j in range(x):
            dist[i, j] = distance_by_rows(mx[:, i, j], dx)
    one_matrix = np.ones_like(dist)
    eye_removed = one_matrix - np.eye(nb_cell)
    _min = np.min(dist, axis=1)
    _min = _min - np.finfo(np.float64).eps
    dist = dist * eye_removed + np.eye(nb_cell) * (_min[..., np.newaxis])
    return dist


def compute_distance_matrix(mx, dx):
    _mx = np.concatenate(([mx[0]], [mx[0]], mx, [mx[-1]], [mx[-1]]))
    # _mx = np.concatenate(([np.zeros_like(mx[0])], [np.zeros_like(mx[0])],
    #                     mx, [np.zeros_like(mx[0])], [np.zeros_like(mx[0])]))

    _mx = _mx.transpose((1, 2, 0)).flatten()
    pos = 0
    pos_up = 0
    pos_down = 0
    nb_cell = mx.shape[-1]
    n = len(dx) * nb_cell ** 2
    n2 = (len(dx) + 1) * nb_cell ** 2
    res = np.empty((n,))
    up = np.empty((n2,))
    down = np.empty((n2,))

    for i in range(0, len(_mx), len(dx) + len(_filter) - 1):
        up[pos_up] = 0
        pos_up += 1
        for j in range(len(dx)):
            row = _mx[i + j: i + j + len(_filter)]
            print(row)
            v = np.dot(row, _filter)
            up[pos_up] = v
            down[pos_down] = v
            res[pos] = v
            pos += 1
            pos_up += 1
            pos_down += 1
        down[pos_down] = 0
        pos_down += 1
    dist = np.empty((nb_cell ** 2))
    amp = (up - down) ** 2
    sign = up * down
    _amp = 0.0
    y, y_p = (0, 0)
    i_t = 0
    pos_dist = 0
    # sign /= np.abs(sign)
    # sign_amp = sign * amp
    for i in range(0, len(sign)):
        if sign[i] < 0 and amp[i] > _amp:
            _amp = amp[i]
            nb_zeros = i // (len(dx) + 1)
            i_t = i - nb_zeros
            y, y_p = res[i_t - 1], res[i_t]
            i_t = i_t % (len(dx))
        if i != 0 and i % (len(dx) + 1) == 0:
            if y == y_p:
                dist[pos_dist] = 1
            else:
                t = - y / (y_p - y)
                sigma = t * (dx[i_t] - dx[i_t - 1]) + dx[i_t - 1]
                dist[pos_dist] = sigma
            pos_dist += 1
            y, y_p = (0, 0)
            _amp = 0
    if y == y_p:
        dist[pos_dist] = 1
    else:
        t = - y / (y_p - y)
        sigma = t * (dx[i_t] - dx[i_t - 1]) + dx[i_t - 1]
        dist[pos_dist] = sigma
    dist = dist.reshape((nb_cell, nb_cell,))
    one_matrix = np.ones_like(dist)
    eye_removed = one_matrix - np.eye(nb_cell)
    # _min = np.min(dist, axis=1)
    # _min = _min - np.finfo(np.float64).eps
    # dist = dist * eye_removed + np.eye(nb_cell) * (_min[..., np.newaxis])
    return dist


def compute_distance_matrix_2(mx, dx):
    # _mx = np.concatenate(([mx[0]], [mx[0]], mx, [mx[-1]], [mx[-1]]))
    _mx = np.concatenate(([mx[0]], [mx[0]], [mx[0]], [mx[0]], mx, [mx[-1]], [mx[-1]], [mx[-1]], [mx[-1]]))

    # _mx = np.concatenate(([np.zeros_like(mx[0])], [np.zeros_like(mx[0])],
    #                     mx, [np.zeros_like(mx[0])], [np.zeros_like(mx[0])]))

    _mx = _mx.transpose((1, 2, 0)).flatten()
    pos = 0
    nb_cell = mx.shape[-1]
    n = len(dx) * nb_cell ** 2
    res = np.empty((n,))

    for i in range(0, len(_mx), len(dx) + len(_filter) - 1):
        for j in range(len(dx)):
            v = np.dot(_mx[i + j: i + j + len(_filter)], _filter)
            res[pos] = v
            pos += 1

    pos_dist = 0
    # sign /= np.abs(sign)
    # sign_amp = sign * amp
    dist = np.empty((nb_cell ** 2))
    scale_const_inv_laplace_deg_4 = 1 / 24 * dx ** 5
    for i in range(0, len(res), len(dx)):
        if np.sum(res[i:i + len(dx)]) == 0.0:
            dist[pos_dist] = 1
        else:
            f_approx = scale_const_inv_laplace_deg_4 \
                       * res[i:i + len(dx)]
            # plt.plot(dx, f_approx)
            # plt.show()
            amax = np.argmax(f_approx)
            dist[pos_dist] = dx[amax]
        pos_dist += 1
    dist = dist.reshape((nb_cell, nb_cell,))
    one_matrix = np.ones_like(dist)
    eye_removed = one_matrix - np.eye(nb_cell)
    # _min = np.min(dist, axis=1)
    # _min = _min - np.finfo(np.float64).eps
    return dist
