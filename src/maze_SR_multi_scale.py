from maze_base import MazeBase
import numpy as np
import findiff
from scipy.special import factorial
import matplotlib.pyplot as plt
from numpy.linalg import inv
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.colors import Normalize

cmap = plt.get_cmap('gist_ncar')


class MazeSRMultiScale(MazeBase):

    def __init__(self, maze_file):
        super().__init__(maze_file)
        self.epsilon = 0.0
        # minimal time bin
        self.t_star_min = 0.0
        # space btw bin
        self.c = 0.0
        # precision of post approximation
        self.k = 0
        # number of sr windows
        self.nb_windows = 0
        # bins
        self.t_star = np.array([], float)
        # sigma association
        self.sigma = np.array([], float)
        # gamma discount factor
        self.discount_sr = np.array([], float)
        # step taken to the maximal possible reward for the run
        # the value is None if the reward taken is not the best possible
        self.max_reward = 0
        self.nu = 0.0
        self.coeff_dist = 0
        self.trace_features_sr = np.array([], float)
        # self.trace_features_sr = np.zeros((self.nb_cells,))

        self.w_ij = np.zeros((self.nb_action, self.nb_cells,))
        self.w_ij_sr = np.array([], float)
        self.distance = np.empty((self.nb_cells, self.nb_cells,), float)
        self.alpha_sr = 0.0
        self.lambda_sr = 0.0
        self.mu = 0.0
        self.w_j = np.zeros((self.nb_cells,))
        self.previous_features = np.zeros((self.nb_cells,))
        self.features = np.zeros((self.nb_cells,))
        self.current_state = self.initial_state
        self.prev_action = np.zeros((self.nb_action,))
        self.signal = 0.0

        self.state = 0.0
        self.state_prime = 0.0

        self.sr_state = np.zeros((self.nb_windows, self.nb_cells,))
        self.sr_state_prime = np.zeros((self.nb_windows, self.nb_cells,))

        self.delta_d = np.zeros((self.nb_cells,))

        self.state_dist = np.zeros((self.nb_cells,))
        self.state_dist_prime = np.zeros((self.nb_cells,))

        self.animate_flag = False

    def set_params(self, params):

        # minimal time bin
        self.t_star_min = params["t_star_min"]
        # space btw bin
        self.c = params["c"]
        # precision of post approximation
        self.k = params["k"]
        # number of sr windows
        self.nb_windows = params["nb_windows"]
        self.nu = params["nu"]
        self.mu = params["mu"]
        self.alpha_sr = params["alpha_sr"]
        self.lambda_sr = params["lambda_sr"]
        self.discount = params["discount"]
        self.t_star = np.array([self.t_star_min * (1 + self.c) ** i for i in range(self.nb_windows)], float)
        self.sigma = self.k / self.t_star
        self.discount_sr = np.exp(-self.sigma)
        self.w_ij_sr = np.tile(np.identity(self.nb_cells, float), (self.nb_windows, 1)).reshape((
            self.nb_windows, self.nb_cells, self.nb_cells))
        self.epsilon = params["epsilon"]

    def update_sr(self, features, sr_state, sr_state_prime):
        delta = features + self.discount_sr[..., np.newaxis] * sr_state_prime - sr_state
        for i in range(self.nb_windows):
            self.w_ij_sr[i] += self.alpha_sr * features[..., np.newaxis] * delta[i]

    def new_sr_state(self, features):
        return np.dot(np.transpose(self.w_ij_sr, (0, 2, 1)), features)

    def compute_distance(self):
        sigmas = self.k / self.t_star
        const_inv_laplace_coef = (-1) ** self.k / factorial(self.k) * sigmas ** (self.k + 1)
        d_dx = findiff.FinDiff(0, self.k)
        for i in range(self.nb_cells):
            for j in range(self.nb_cells):
                self.distance[:, i, j] = \
                    const_inv_laplace_coef * d_dx(self.w_ij_sr[:, i, j])

    def update(self, sample_maze: bool):
        self.update_sr(self.previous_features, self.sr_state, self.sr_state_prime)
        if not sample_maze:
            self.compute_distance()

    def set_features(self, state):
        self.features[state] = 1

    def new_features(self, state, action):
        state_p = self.tr_matrix[state, action]
        self.previous_features = self.features
        features = np.zeros((self.nb_cells,))
        features[state_p] = 1
        self.current_state = state_p
        return features

    def run(self, step, output, sample_maze, results_handler, animation):
        self.set_features(self.current_state)
        i = 1
        if step == -1:
            step = float('inf')
        if not sample_maze:
            self.compute_distance()

        if animation and not sample_maze:
            self.animate()
            for j in range(self.nb_cells):
                fig1, axs1 = plt.subplots(2)
                legend = []
                line_norm = Normalize(0, self.nb_cells*10)
                for i in range(self.nb_cells):
                    if i == j \
                            or np.sum(self.w_ij_sr[:, i, j]) == self.nb_cells \
                            or np.sum(self.w_ij_sr[:, i, j]) == 0.0:
                        continue
                    norm_value = line_norm(i*10)
                    col = cmap(norm_value)
                    axs1[0].plot(self.discount_sr, self.w_ij_sr[:, j, i], c=col)
                    pair_dist = self.distance[:, j, i]
                    print(f"argmax = {np.argmax(pair_dist)}")
                    print(f"gamma = {np.exp(- self.k/self.t_star[np.argmax(pair_dist)])}")
                    axs1[1].plot(self.t_star, pair_dist / np.linalg.norm(pair_dist), c=col)
                    legend.append(f"{j}_{i}")
                axs1[0].legend(legend, ncol=self.width)
                axs1[1].legend(legend, ncol=self.width)
                fig1.set_size_inches(18.5, 10.5)
                with PdfPages(f'simple_latent_maze_{j}_maze.pdf') as pdf:
                    plt.savefig(pdf, dpi=None, facecolor='w', edgecolor='w',
                                orientation='landscape', papertype=None, format='pdf',
                                transparent=False, bbox_inches='tight', pad_inches=0.1,
                                frameon=None, metadata=None)

        self.sr_state = self.new_sr_state(self.features)
        self.action = self.new_possible_random_action()
        self.features = self.new_features(self.current_state, self.action)
        self.sr_state_prime = self.new_sr_state(self.features)
        self.update(sample_maze)
        while i < step:
            print(i)
            i += 1
            self.sr_state = self.sr_state_prime
            self.action = self.new_possible_random_action()
            self.features = self.new_features(self.current_state, self.action)

            self.sr_state_prime = self.new_sr_state(self.features)
            self.update(sample_maze)
            if output:
                results_handler.parse_results_between_cycle(self)
        self.max_step = i
        return i
