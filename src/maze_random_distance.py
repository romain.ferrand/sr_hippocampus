from maze_actor_critic import MazeActorCritic
from maze_base import MazeBase
import numpy as np
import utils


class MazeRandomDistance(MazeActorCritic):
    def __init__(self, maze_file):
        super().__init__(maze_file)
        # self.nb_windows = 9
        # self.step_windows = 0.1
        # self.discount_sr = [self.step_windows * i for i in range(1, self.nb_windows + 1)]
        self.discount_sr = np.linspace(start=0, stop=1, num=10, endpoint=False)
        self.nb_windows = len(self.discount_sr)
        self.distance = np.empty((self.nb_cells, self.nb_cells,), float)
        self.w_ij_sr = np.tile(np.identity(self.nb_cells, float), (self.nb_windows, 1)).reshape((
            self.nb_windows, self.nb_cells, self.nb_cells))

        self.initial_state = -1
        self.current_state = -1
        # params
        self.alpha_sr = 0.0

        self.state = 0.0
        self.state_prime = 0.0
        self.dist = 0.0
        self.dist_prime = 0.0

        # features vector
        self.previous_features = np.zeros((self.nb_cells,))
        self.features = np.zeros((self.nb_cells,))

        self.sr_state = np.zeros((self.nb_windows, self.nb_cells,))
        self.sr_state_prime = np.zeros((self.nb_windows, self.nb_cells,))

        self.state_dist = np.zeros((self.nb_cells,))
        self.state_dist_prime = np.zeros((self.nb_cells,))

    def update(self, sample_maze):
        self.update_sr()
        if not sample_maze:
            self.compute_distance()

    def v_value(self, features):
        return np.dot(self.w_j, features)
        # print("v_value {}".format(v_value))

    def set_params(self, params):
        self.alpha_sr = params["alpha_sr"]

    def update_sr(self):
        # print(self.previous_features)
        for i in range(self.nb_windows):
            update_sr_wij = self.previous_features + self.discount_sr[i] * self.sr_state_prime[i] - self.sr_state[i]
            self.w_ij_sr[i] += self.alpha_sr * self.previous_features[..., np.newaxis] * update_sr_wij

    def new_distance(self, features):

        # dist = np.sum(self.distance * features[..., np.newaxis], axis=0)
        dist = np.dot(self.distance.transpose(), features)
        # state_dist = rankdata(state_dist)
        # state_dist = 5 / (np.max(state_dist) - state_dist + 1)
        # state_dist = gaussian_filter1d(state_dist, 0.5)

        return dist

    def new_sr_state(self):
        return np.dot(np.transpose(self.w_ij_sr, (0, 2, 1)), self.features)

    def compute_distance(self):
        # print("current cell = {}".format(self.current_state))
        self.distance = utils.compute_distance_matrix(self.w_ij_sr, self.discount_sr)
        # _norm = np.linalg.norm(self.distance, axis=1)
        # self.distance /= _norm[..., np.newaxis]
        # _min = np.min(self.distance, axis=1)
        # _max = np.max(self.distance, axis=1)
        # delta = _max - _min
        # self.distance = _max[..., np.newaxis] * ((self.distance - _min[..., np.newaxis]) / delta[..., np.newaxis])
        return self.distance

    def reset_maze(self):
        super(MazeRandomDistance, self).reset_maze()
        self.state = np.zeros((self.nb_windows,))
        self.state_prime = np.zeros((self.nb_windows,))
        self.features = np.zeros((self.nb_cells,))
        self.previous_features = np.zeros((self.nb_cells,))
        self.sr_state = np.zeros((self.nb_windows, self.nb_cells,))
        self.sr_state_prime = np.zeros((self.nb_windows, self.nb_cells,))

    def run(self, step, output, sample_maze, results_handler, animation):
        if animation:
            self.animate()
        i = 1
        # step = -1 imply end on reward condition so step is unbounded
        if step == -1:
            step = float('inf')
        self.set_features(self.current_state)
        # handle the first (S,A,S') observation
        # observe state S
        self.sr_state = self.new_sr_state()
        self.state_dist = self.new_distance(self.features)
        self.state = self.v_value(self.features)


        # new action
        q_values = self.eval(self.features)
        self.action = self.new_action(q_values, sample_maze)
        self.features, bump_to_wall = self.new_features(self.current_state, self.action)

        # observe state S'
        self.sr_state_prime = self.new_sr_state()
        self.state_dist_prime = self.new_distance(self.features)
        self.state_prime = self.v_value(self.features)
        self.current_reward = self.new_reward(self.features, sample_maze, bump_to_wall)

        self.signal = self.new_signal(self.state, self.state_prime)

        # update
        print(i)
        self.update(sample_maze)

        if animation:
            self.animate()
            print("state = {}".format(self.state))
            print("state_prime = {}".format(self.state_prime))
        if output:
            results_handler.parse_results_between_cycle(self)
        while i < step and self.current_reward <= 0.0:
            i += 1
            self.state = self.state_prime
            self.sr_state = self.sr_state_prime
            self.state_dist = self.state_dist_prime

            # new action
            q_values = self.eval(self.features)
            self.action = self.new_action(q_values, sample_maze)

            self.features, bump_to_wall = self.new_features(self.current_state, self.action)

            # observe state S'
            self.sr_state_prime = self.new_sr_state()
            self.state_dist_prime = self.new_distance(self.features)
            self.state_prime = self.v_value(self.features)

            self.current_reward = self.new_reward(self.features, sample_maze, bump_to_wall)
            self.signal = self.new_signal(self.state, self.state_prime)

            # update
            print(i)
            self.update(sample_maze)
            if animation:
                self.animate()
                print("state = {}".format(self.state))
                print("state_prime = {}".format(self.state_prime))
            # results parsing
            if output:
                results_handler.parse_results_between_cycle(self)
        return i

