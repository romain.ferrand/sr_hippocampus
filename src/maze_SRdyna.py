from maze_base import MazeBase
import numpy as np


def exponential(x, mu):
    return (1 / mu) * np.exp(-x / mu)


class MazeSRDyna(MazeBase):

    def __init__(self, maze_file):
        super().__init__(maze_file)
        self.gamma_sr, self.discount, self.nb_sample = (0, 0, 0)
        self.nb_as = self.nb_cells * self.nb_action
        self.sr_matrix = np.identity(self.nb_as, float)
        self.Q = np.empty((self.nb_as,))
        self.w = np.empty((self.nb_as,))
        self.sa_samples = [[] for _ in range(self.nb_as)]
        self.sa_list = []
        self.pdf = exponential(np.arange(100), 5)
        self.pdf = self.pdf / np.sum(self.pdf)

    def eval(self, state):
        self.Q = np.dot(self.sr_matrix, self.w)
        n_as = self.nb_action * state
        asa = np.array([n_as, n_as + 1, n_as + 2, n_as + 3])
        return self.Q[asa]

    def sarsa_update(self):
        nas = self.nb_action * self.state + self.action
        nas_prime = self.nb_action * self.state_prime + self.action_prime
        t = np.zeros((self.nb_as,))
        t[nas] = 1
        self.sr_matrix[nas, :] = \
            (1 - self.gamma_sr) * self.sr_matrix[nas, :] + self.gamma_sr * (
                    t + self.discount * self.sr_matrix[nas_prime, :])

    def params_to_dictionary(self, res):
        res["gamma_sr"] = self.gamma_sr
        res["gamma_td"] = self.gamma_td
        res["discount"] = self.discount
        res["epsilon"] = self.epsilon
        res["nb_sample"] = self.nb_sample

    def set_params(self, params):
        self.gamma_td = params["gamma_td"]
        self.gamma_sr = params["gamma_sr"]
        self.discount = params["discount"]
        self.epsilon = params["epsilon"]
        self.nb_sample = params["nb_sample"]

    def new_w(self):
        nas = self.nb_action * self.state + self.action
        nas_prime = self.nb_action * self.state_prime + self.action_prime
        feature_prev = self.sr_matrix[nas, :]
        feature_prev_norm = np.dot(feature_prev, feature_prev)
        td_update_w = \
            self.current_reward + self.discount * np.dot(self.sr_matrix[nas_prime, :], self.w) \
            - np.dot(self.sr_matrix[nas, :], self.w)
        self.w += self.gamma_td * td_update_w * (feature_prev / feature_prev_norm)

    def dyna_update(self):
        u_sa = np.unique(self.sa_list)
        for k in range(self.nb_sample):
            ind = np.random.randint(0, len(u_sa))
            sa_num = u_sa[ind]
            n = np.random.choice(np.arange(100), size=1, p=self.pdf)[0]
            nb_sample = len(self.sa_samples[sa_num]) - 1
            if n > nb_sample:
                n = nb_sample

            sample = self.sa_samples[sa_num][nb_sample - n]
            s = sample[0]
            a = sample[1]
            r = sample[2]
            s_prime = sample[3]
            a_prime = sample[4]
            self.qlearn_update(s, a, r, s_prime, a_prime)
            self.Q = np.dot(self.sr_matrix, self.w)

    def update(self):
        self.sarsa_update()
        self.new_w()
        sa_num = self.state + self.action
        self.sa_samples[sa_num].append([self.state,
                                        self.action,
                                        self.current_reward,
                                        self.state_prime,
                                        self.action_prime])
        self.sa_list.append(sa_num)
        self.dyna_update()

    def qlearn_update(self, s, a, r, s_prime, a_prime):
        sa = s * self.nb_action + a
        n_sprime = s_prime * self.nb_action
        available_sa_prime = np.array([n_sprime, n_sprime + 1, n_sprime + 2, n_sprime + 3])
        q_values = self.Q[available_sa_prime]
        q_max = np.max(q_values)
        ind_q_max = np.argwhere(q_values == q_max)[0]
        if len(ind_q_max) > 1:
            a_prime_star = a_prime
        else:
            a_prime_star = ind_q_max[0]

        sa_prime_star = s_prime * self.nb_action + a_prime_star
        t = np.zeros((self.nb_as,))
        t[sa] = 1
        self.sr_matrix[sa, :] = \
            (1 - self.gamma_sr) * self.sr_matrix[sa, :] \
            + self.gamma_sr * (t + self.discount * self.sr_matrix[sa_prime_star, :])

    def reset_maze(self):
        super(MazeSRDyna, self).reset_maze()
        self.sa_samples = [[] for _ in range(self.nb_as)]
        self.sa_list = []

    def run(self, step, animation):
        i = super(MazeSRDyna, self).run(step, animation)
        self.dyna_update()
        return i + 1

    def output_data_results(self, res):
        tr_m = self.tr_matrix.copy()
        sr_m = self.sr_matrix.copy()
        q_values = self.Q.copy()
        v_value = []
        for i in range(0, len(q_values), 5):
            i_q_vals = q_values[i:i + 5]
            ind = np.nonzero(i_q_vals)[0]
            if len(ind) > 0:
                v = (sum(i_q_vals)) / len(ind)
            else:
                v = 0.0
            v_value.append(v)
        w = self.w.copy()
        r = self.rewards.copy()
        self.params_to_dictionary(res),
        res["SR_matrix"] = sr_m.tolist(),
        res["V_values"] = v_value,
        res["W_values"] = w.tolist(),
        res["R_values"] = r.tolist(),
        res["Maze_layout"] = tr_m.tolist()
