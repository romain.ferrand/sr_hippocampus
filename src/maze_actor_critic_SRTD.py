from maze_actor_critic import MazeActorCritic
import numpy as np
import sys
import numpy
numpy.set_printoptions(threshold=sys.maxsize)


class MazeActorCriticSRTD(MazeActorCritic):

    def update(self, sample_maze):
        self.update_sr()
        if not sample_maze:
            self.update_actor()
            self.update_critic()

    def __init__(self, maze_file):
        super().__init__(maze_file)
        self.max_reward = 0
        self.w_ij_sr = np.eye(self.nb_cells)
        self.sr_state = np.zeros((self.nb_cells,))
        self.sr_state_prime = np.zeros((self.nb_cells,))
        self.trace_features = np.zeros((self.nb_cells,))
        self.alpha_sr = 0.0
        self.lambda_sr = 0.0

    def new_sr_state(self, features):
        return np.dot(self.w_ij_sr.transpose(), features)

    def set_params(self, params):
        self.nu = params["nu"]
        self.discount = params["discount"]
        self.alpha_sr = params["alpha_sr"]
        self.lambda_sr = params["lambda_sr"]

    def update_sr(self):
        self.trace_features = self.discount * self.lambda_sr * \
                              self.trace_features + self.previous_features
        update_sr_wij = self.previous_features + self.discount * self.sr_state_prime - self.sr_state
        self.w_ij_sr += self.alpha_sr * self.previous_features[..., np.newaxis] * update_sr_wij

    def update_critic(self):
        total = np.dot(self.sr_state, self.sr_state)
        norm = self.previous_features if total == 0 else self.sr_state / total
        self.w_j += self.nu * self.signal * norm

    def update_actor(self):
        self.w_ij[self.action] += self.nu * self.signal * self.previous_features

    def v_value(self, sr_state):
        return np.dot(sr_state, self.w_j)

    def reset_maze(self):
        super(MazeActorCriticSRTD, self).reset_maze()
        self.state = 0
        self.state_prime = 0
        self.features = np.zeros((self.nb_cells,))
        self.previous_features = np.zeros((self.nb_cells,))
        self.sr_state = 0
        self.sr_state_prime = 0

    def run(self, step, output, sample_maze, results_handler, animation):
        if animation:
            self.animate()
        i = 1
        # step = -1 imply end on reward condition so step is unbounded
        if step == -1:
            step = float('inf')
        self.set_features(self.current_state)

        # handle the first (S,A,S') observation
        # observe state S
        self.sr_state = self.new_sr_state(self.features)
        if not sample_maze:
            self.state = self.v_value(self.sr_state)

        # new action
        if step == 1:
            reward = np.dot(self.features, self.rewards)
            if reward > 0:
                self.action = self.nb_action - 1
                self.current_reward = reward
            else:
                q_values = self.new_q(self.features)
                self.action = self.new_action(q_values, sample_maze)
                self.features = self.new_features(self.current_state, self.action)

                if not sample_maze:
                    self.current_reward = self.new_reward(self.features, sample_maze)

        # observe state S'
        self.sr_state_prime = self.new_sr_state(self.features)
        if not sample_maze:
            self.state_prime = self.v_value(self.sr_state_prime)

            self.signal = self.new_signal(self.state, self.state_prime)

        self.update(sample_maze)
        if output:
            results_handler.parse_results_between_cycle(self)

        while i < step and self.current_reward <= 0.0 and (i <= 1000 or sample_maze):
            i += 1
            self.sr_state = self.sr_state_prime
            if not sample_maze:
                self.state = self.state_prime
            # new action
            q_values = self.new_q(self.features)
            self.action = self.new_action(q_values, sample_maze)
            self.features = self.new_features(self.current_state, self.action)

            if not sample_maze:
                # new reward
                self.current_reward = self.new_reward(self.features, sample_maze)

            # observe state S'
            self.sr_state_prime = self.new_sr_state(self.features)
            if not sample_maze:
                self.state_prime = self.v_value(self.sr_state_prime)
                self.signal = self.new_signal(self.state, self.state_prime)
            # update
            self.update(sample_maze)

        self.max_step = i
        if not sample_maze:
            self.max_reward = self.current_reward \
                if self.current_reward == np.max(self.rewards) else None
        print(f"max_step = {self.max_step}")
        print(f"max_reward = {self.max_reward}")
        return i
