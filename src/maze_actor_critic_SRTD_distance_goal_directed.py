from maze_actor_critic_SRTD_distance import MazeActorCriticSRTDDistance
import numpy as np


class MazeSRDistanceGD(MazeActorCriticSRTDDistance):
    def __init__(self, maze_file):
        super(MazeSRDistanceGD, self).__init__(maze_file)

    def new_signal(self, state, state_prime):
        return state - state_prime

    def update_actor(self):
        update = self.nu * self.signal * self.previous_features
        self.w_ij[self.action] += update

    def set_params(self, params):
        super(MazeSRDistanceGD, self).set_params(params)
        self.w_j = np.array(params["wanted_feature"])

    def new_distance(self, features):
        dist = np.dot(self.distance.transpose(), features)
        return dist

    def update_critic(self):
        pass

    def update(self, sample_maze):
        self.update_sr()
        if not sample_maze:
            self.update_actor()
            self.compute_distance()

    def new_reward(self, features, sample_maze):
        return 0

    def run(self, step, output, sample_maze, results_handler, animation):
        self.animate_flag = True if animation and not sample_maze else False
        self.animate()
        i = 1
        # step = -1 imply end on reward condition so step is unbounded
        if step == -1:
            step = float('inf')
        self.set_features(self.current_state)

        if not sample_maze:
            self.compute_distance()
        # handle the first (S,A,S') observation

        # observe state S
        self.sr_state = self.new_sr_state(self.features)
        if not sample_maze:
            self.state_dist = self.new_distance(self.features)
            self.state = self.v_value(self.state_dist)

        # new action
        if step == 1:
            reward = np.dot(self.features, self.rewards)
            if reward > 0:
                self.action = self.nb_action - 1
                self.current_reward = reward
            else:
                q_values = self.eval(self.features)
                self.action = self.new_action(q_values, sample_maze)
                self.features = self.new_features(self.current_state, self.action)

                if not sample_maze:
                    self.current_reward = self.new_reward(self.features, sample_maze)

        # observe state S'
        self.sr_state_prime = self.new_sr_state(self.features)
        if not sample_maze:
            self.state_dist_prime = self.new_distance(self.features)
            self.state_prime = self.v_value(self.state_dist_prime)
            self.signal = self.new_signal(self.state, self.state_prime)

        # update
        self.update(sample_maze)

        self.animate()

        if output:
            results_handler.parse_results_between_cycle(self)
        while i < step and (not np.array_equal(self.features, self.w_j) or sample_maze) and (i <= 1000 or sample_maze):
            self.animate_flag = True if animation and not sample_maze else False
            i += 1
            print(f"run = {i}")
            self.sr_state = self.sr_state_prime
            if not sample_maze:
                self.state_dist = self.state_dist_prime
                self.state = self.state_prime

            # new action
            q_values = self.eval(self.features)
            self.action = self.new_action(q_values, sample_maze)
            self.features = self.new_features(self.current_state, self.action)
            self.current_reward = self.new_reward(self.features, sample_maze)

            # observe state S'
            self.sr_state_prime = self.new_sr_state(self.features)

            if not sample_maze:
                self.state_dist_prime = self.new_distance(self.features)
                self.state_prime = self.v_value(self.state_dist_prime)
                self.signal = self.new_signal(self.state, self.state_prime)

            # update
            self.update(sample_maze)
            self.animate()

        if output:
            results_handler.parse_results_between_cycle(self)
        self.max_step = i
        if not sample_maze:
            self.max_reward = self.current_reward if self.current_reward == np.max(self.rewards) else None
        print(f"max_step = {self.max_step}")
        print(f"max_reward = {self.max_reward}")
        return i
