"""
Class for output results inside mongodb or json file
"""
from abc import ABC
from typing import Union, Any, List, Optional, cast, Dict
import numpy as np
import json
import utils
import time


class ResultHandler(ABC):
    # data stored between phase
    between_phase: Dict[str, list]
    # Union[list, float, List[float], List[List[float]], List[List[List[float]]]]]]
    # data stored between cycle
    between_cycle: Dict[str, list]

    # Union[list, float, List[float], List[List[float]], List[List[List[float]]]]]]]
    # data stored for general parameters (maze name, exp name, maze_file, ...)
    general_params: Dict[str, str]

    # Union[list, float, List[float], List[List[float]], List[List[List[float]]]]]]]
    # number of phases
    phase: int
    # number of cycles
    cycle: int
    # timestamp
    timestamp = str

    # construct
    def __init__(self, general_params: List[str], phase_params: List[str], cycle_params: List[str]) -> None:
        self.general_params = dict()
        self.between_phase = dict()
        self.between_cycle = dict()
        self.phase = 0
        self.cycle = 0
        for param in phase_params:
            self.between_phase[param] = []
        for param in cycle_params:
            self.between_cycle[param] = []
        for param in general_params:
            self.general_params[param] = ""

        self.timestamp = time.strftime("%Y-%m-%d_%H-%M-%S")

    def parse_general_parameters(self, maze_handler: object):
        for k in self.general_params.keys():
            value = maze_handler.__dict__[k]
            self.general_params[k] = value
            print(self.general_params[k])

    def parse_results_between_phase(self, maze: object) -> None:
        self.phase += 1
        for k in self.between_phase.keys():
            value = maze.__dict__[k]
            if isinstance(value, np.ndarray):
                value = value.tolist()
            self.between_phase[k].append(value)

    def parse_results_between_cycle(self, maze: object) -> None:
        self.cycle += 1
        for k in self.between_cycle.keys():
            value = maze.__dict__[k]
            if isinstance(value, np.ndarray):
                value = value.tolist()
            self.between_cycle[k].append(value)

    def output_results(self, is_output_json: bool) -> None:
        path = "./results/" + self.general_params["simulation_name"] + " " + self.timestamp
        json_file = self.results_to_json()
        print(json_file)
        if is_output_json:
            with open(path + ".json") as json_data:
                json.dump(json_file, json_data)
        else:

            db = utils.mongo_connect()
            rec = db.results.simulation_data.insert_one(json_file)
            print(rec)

    def results_to_json(self) -> Dict[str, Union[list, dict]]:
        json_file = dict()
        for k, v in self.general_params.items():
            json_file[k] = v
        json_file["between_phase_results"] = self.between_phase
        json_file["between_cycle_results"] = self.between_cycle
        # print(json_file["between_phase_results"]["distance"])
        return json_file
