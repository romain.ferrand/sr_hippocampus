import os
import json
import time
from collections import deque
import numpy as np
import maze_SRdyna
import maze_SRtd
import maze_SRmb
import maze_TDlambda
import maze_actor_critic
import maze_actor_critic_SRTD
import maze_actor_critic_SRTD_windows
import maze_actor_critic_SRTD_distance
import maze_random_distance
import maze_actor_critic_SRTD_distance_goal_directed
import maze_actor_critic_SRTD_goal_directed
import maze_SR_multi_scale
from numpy.linalg.linalg import LinAlgError
import utils
import stopit
import results_handler


class MazeHandler:

    def __init__(self, cf):
        general_params = ["method_name",
                          "simulation_name",
                          "initial_parameters",
                          "maze_file", "seed"]
        print(cf)
        self.timestr = time.strftime("%Y-%m-%d_%H-%M-%S")
        # parse simulation data
        sim = cf["simulation_data"]
        # choose parameters values to output
        self.results_handler = results_handler.ResultHandler(general_params,
                                                             sim["phase_params"],
                                                             sim["cycle_params"])
        #
        self.method_type = sim["method_type"]
        self.method_name = self.method_type["name"]
        self.initial_parameters = self.method_type["initial_parameters"]
        self.simulation_name = sim["simulation_name"]
        self.maze_file = sim["maze"]
        self.events = deque(sim["exp_events"])
        self.simulation_id = sim["simulation_name"] + "_" + self.timestr
        self.seed = sim["seed"]
        self._dir = "results/{}_{}".format(
            sim["simulation_name"], self.timestr)
        if self.method_type["name"] == "SR-TD":
            self.maze = maze_SRtd.MazeTD(
                self.maze_file)
        elif self.method_type["name"] == "SR-MB":
            self.maze = maze_SRmb.MazeMB(
                self.maze_file)
        elif self.method_type["name"] == "SR-DYN":
            self.maze = maze_SRdyna.MazeSRDyna(
                self.maze_file)
        elif self.method_type["name"] == "TD":
            self.maze = maze_TDlambda.MazeTDLambda(
                self.maze_file)
        elif self.method_type["name"] == "AC":
            self.maze = maze_actor_critic.MazeActorCritic(self.maze_file)
        elif self.method_type["name"] == "AC_SR-TD":
            self.maze = maze_actor_critic_SRTD.MazeActorCriticSRTD(
                self.maze_file)
        elif self.method_type["name"] == "AC_SR-TD_windows":
            self.maze = maze_actor_critic_SRTD_windows.MazeActorCriticSRTDWindows(
                self.maze_file)
        elif self.method_type["name"] == "AC_SR-TD_distance":
            self.maze = maze_actor_critic_SRTD_distance.MazeActorCriticSRTDDistance(
                self.maze_file)
        elif self.method_type["name"] == "maze_random_distance":
            self.maze = maze_random_distance.MazeRandomDistance(self.maze_file)
        elif self.method_type["name"] == "maze_AC_goal_directed":
            self.maze = maze_actor_critic_SRTD_distance_goal_directed.MazeSRDistanceGD(self.maze_file)
        elif self.method_type["name"] == "maze_AC_SR_goal_directed":
            self.maze = maze_actor_critic_SRTD_goal_directed.MazeSRGD(self.maze_file)
        elif self.method_type["name"] == "maze_SR_multi_scale":
            self.maze = maze_SR_multi_scale.MazeSRMultiScale(self.maze_file)
        self.maze.set_params(self.initial_parameters)
        try:
            with stopit.ThreadingTimeout(30000) as to_ctx_mgr:
                assert to_ctx_mgr.state == to_ctx_mgr.EXECUTING
                print(sim["animated"])
                self.run_experiment(sim["animated"], self.seed)
            if to_ctx_mgr.state == to_ctx_mgr.TIMED_OUT:
                print("Timeout")
                return
        except (LinAlgError, ZeroDivisionError):
            # silence error the maze evaluation is canceled
            return

    def run_experiment(self, animated, seed):
        np.random.seed(seed)
        self.results_handler.parse_general_parameters(self)
        while self.events:
            new_event = self.events.popleft()
            event_type = list(new_event.keys())[0]
            event_data = new_event[event_type]

            # sample model's dynamic
            """
            if event_type == "sample_model":
                positions = event_data["initial_pos"]
                for k in range(event_data["samples"]):
                    self.maze.reset_maze()
                    i, j = positions[k % len(positions)]
                    self.maze.set_initial_state(i, j)
                    nb_run = self.maze.sample_model(event_data["max_step"],
                                                    event_data["output"],
                                                    self.results_handler,
                                                    animated)
                    self.maze.max_step = nb_run

                    # output results between phase
                    self.results_handler.parse_results_between_phase(self.maze)
            """
            # sample run to reward
            if event_type == "run_phase":
                positions = event_data["initial_pos"]
                for k in range(event_data["samples"]):
                    self.maze.reset_maze()
                    i, j = positions[k % len(positions)]
                    self.maze.set_initial_state(i, j)
                    nb_run = self.maze.run(event_data["max_step"],
                                           event_data["output"],
                                           event_data["sample_maze"],
                                           self.results_handler,
                                           animated)
                    self.maze.max_step = nb_run
                    if event_data["output"]:
                        self.results_handler.parse_results_between_phase(self.maze)

            elif event_type == "reward_change":
                self.maze.set_new_reward(event_data["cell_pos"],
                                         event_data["value"])

            elif event_type == "wall_change":
                i, j = event_data["cell_pos"]
                wall_dir = event_data["wall_dir"]
                wall_status = event_data["wall_status"]
                self.maze.modify_wall(i, j, wall_dir, wall_status)

            elif event_type == "params_change":
                for k, v in event_data.items():
                    setattr(self.maze, k, v)

        # output results in json file on in mongo database
        self.results_handler.output_results(is_output_json=False)

    @staticmethod
    def create_dir(dirname):
        if not os.path.exists(dirname):
            os.mkdir(dirname)
            print("Directory ", dirname, " Created ")
        else:
            print("Directory ", dirname, " already exists")

    @staticmethod
    def parse_json(config):
        with open(config) as json_data:
            d = json.load(json_data)
        return d


if __name__ == '__main__':
    cfg = MazeHandler.parse_json("./config/detour_maze_SR-TD.json")
    MazeHandler(cfg)

    """
    def have_converged_rec(self, flag, n, reward, mask):
        if mask[n] == 0:
            mask[n] = 1
            if n == reward:
                flag[0] = True
                return
            else:
                [self.have_converged_rec(flag, x, reward, mask)
                 for x in self.maze.tr_matrix[n, :] if self.maze.v_func[x] > self.maze.v_func[n]]

    def have_converged(self):
        mask = np.zeros((self.maze.nb_cells,))
        cv = [False]
        i, j = self.method_params["reward_pos"]
        reward = self.maze.ij_to_number(i, j)
        start_pos = [self.maze.ij_to_number(i, j) for i, j in self.method_params["start_pos"]]
        flag = True
        for x in start_pos:
            self.have_converged_rec(cv, x, reward, mask)
            print(cv[0])
            flag = flag and cv[0]
        self.have_conv = flag
    """
