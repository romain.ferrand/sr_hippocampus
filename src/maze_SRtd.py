from maze_base import MazeBase
import numpy as np


class MazeTD(MazeBase):
    def __init__(self, maze_file):
        super().__init__(maze_file)
        self.gamma_sr, self.discount = (0, 0)

    def update(self, sample_maze):
        self.new_sr_matrix()
        if not sample_maze:
            self.new_w()

    def eval(self, state):
        self.new_v()
        return self.new_q(state)

    def set_params(self, params):
        self.gamma_td = params["gamma_td"]
        self.gamma_sr = params["gamma_sr"]
        self.discount = params["discount"]
        self.epsilon = params["epsilon"]

    def new_sr_matrix(self):
        ident_func = np.zeros(self.nb_cells)
        ident_func[self.state] = 1
        self.sr_matrix[self.state, :] = \
            (1 - self.discount) * self.sr_matrix[self.state, :] \
            + self.discount * (ident_func + self.gamma_sr * self.sr_matrix[self.state_prime, :])
