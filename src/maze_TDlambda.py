from maze_base import MazeBase
import numpy as np


class MazeTDLambda(MazeBase):

    def eval(self, state):
        return self.new_q(state)

    def set_params(self, params):
        self.gamma_td = params["gamma_td"]
        self.gamma_e = params["gamma_e"]
        self.discount = params["discount"]
        self.epsilon = params["epsilon"]

    def new_q(self, state):
        cells_number = self.tr_matrix[state, :]
        return self.v_func[cells_number]

    def new_v(self):
        v_update = self.current_reward + self.discount * self.v_func[self.state_prime] - self.v_func[self.state]
        self.v_func += self.gamma_td * v_update * self.w

    def update(self):
        self.new_w()
        self.new_v()

    def new_w(self):
        outcome = np.zeros((self.nb_cells,))
        outcome[self.state] = 1
        self.w = self.discount * self.gamma_e * self.w + outcome

    def reset_maze(self):
        super(MazeTDLambda, self).reset_maze()
        self.w = np.zeros((self.nb_cells,))

    def __init__(self, maze_file, params):
        super().__init__(maze_file)
        self.gamma_e = 0.0
        self.set_params(params)
