from abc import ABC, abstractmethod
import time
import sys
import numpy as np
import utils


class MazeBase(ABC):
    def __init__(self, maze_file):
        # maximum step of the current phase (useful for data parsing)
        self.max_step = 0
        self.gamma_td, self.discount, self.epsilon = (0, 0, 0)
        self.height, self.width = maze_file["height"], maze_file["width"]
        self.nb_cells = self.height * self.width
        # 5 action up = 0, down = 1, left = 2, right = 3, stay = 4
        self.nb_action = 5
        self.tr_matrix = np.empty((self.nb_cells, self.nb_action), dtype=int)
        self.create_tr_matrix()
        self.create_walls(maze_file["path"])
        self.state_prime = -1
        self.state = -1
        self.initial_state = -1
        self.current_state = self.state
        self.sr_matrix = np.identity(self.nb_cells, float)
        self.v_func = np.zeros(self.nb_cells, )
        self.w = np.zeros(self.nb_cells)
        self.rewards = np.zeros(self.nb_cells, )
        self.action_prime = -1
        self.action = -1
        self.current_reward = 0.0
        self.animate_flag = False


    @abstractmethod
    def set_params(self, params):
        pass

    @abstractmethod
    def eval(self, state):
        pass

    @abstractmethod
    def update(self, sample_maze: bool):
        pass

    """
    Sample model dynamic (update SR matrix)

    """

    def new_state(self, state, action):
        state_prime = self.tr_matrix[state, action]
        return state_prime

    def new_reward(self, state, sample_maze):
        if sample_maze:
            return 0
        else:
            reward = self.rewards[state]
            if reward > 0.0:
                return reward
            else:
                return -1

    def run(self, step, output, sample_maze, results_handler, animation):
        if animation:
            self.animate()
        i = 1

        # if step is -1 the run end on reward
        if step == -1:
            step = float('inf')
        if step == 1:
            reward = self.rewards[self.current_state]
            if reward > 0:
                self.action = self.nb_action - 1
                self.current_reward = reward
                self.state_prime = self.state
            else:
                q_values = self.eval(self.state)
                self.action = self.new_action(q_values, sample_maze)
                self.state_prime = self.new_state(self.state, self.action)
                if not sample_maze:
                    self.current_reward = self.new_reward(self.current_state, sample_maze)
        print(i)
        self.update(sample_maze)
        if output:
            results_handler.parse_results_between_cycle(self)

        while i < step and self.current_reward <= 0.0:
            i += 1
            self.state = self.state_prime
            # new action
            q_values = self.eval(self.state)
            self.action = self.new_action(q_values, sample_maze)
            self.state_prime = self.new_state(self.state, self.action)
            if not sample_maze:
                self.current_reward = self.new_reward(self.state_prime, sample_maze)
                # change that later...
                if self.current_reward > 0.0:
                    reward_temp = self.current_reward
                    self.current_reward = -1
                    self.update(sample_maze)
                    self.state = self.state_prime
                    self.action = self.nb_action - 1
                    self.current_reward = reward_temp
            print(i)
            self.update(sample_maze)
            if animation:
                self.animate()
            if output:
                results_handler.parse_results_between_cycle(self)
        self.max_step = i
        return i

    def reset_maze(self):
        self.action_prime = -1
        self.action = -1
        self.state = -1
        self.state_prime = -1
        self.initial_state = -1
        self.current_state = -1
        self.current_reward = 0.0

    def new_q(self, state):
        cells_number = self.tr_matrix[state, :-1]

        return self.v_func[cells_number]

    def new_v(self):
        self.v_func = np.dot(self.sr_matrix, self.w)

    def new_action(self, q_values, sample_maze):
        if sample_maze:
            return self.new_possible_random_action()
        q_values = np.append(q_values, 0.0)
        action_prob = np.zeros_like(q_values)
        q_max = np.max(q_values)
        ind_q_max = np.argwhere(q_values == q_max).flatten()
        nb_max = len(ind_q_max)
        action_prob[ind_q_max] = (1 - self.epsilon) / nb_max
        action_prob += self.epsilon / len(q_values)
        return np.random.choice(range(self.nb_action), 1, p=action_prob)[0]

    """
    compute new action with wall consideration
    """

    def new_possible_action(self, q_values):
        action_prob = np.zeros_like(q_values)
        impossible_action = np.array([i for i, x in enumerate(self.tr_matrix[self.current_state, :])
                                      if x == self.current_state and i != self.nb_action - 1])
        q_values[impossible_action] = 0.0
        q_max = np.max(q_values)
        ind_q_max = np.argwhere(q_values == q_max).flatten()
        nb_max = len(ind_q_max)
        action_prob[ind_q_max] = (1 - self.epsilon) / nb_max
        action_prob += self.epsilon / len(q_values)
        return np.random.choice(range(self.nb_action), 1, p=action_prob)[0]

    def new_random_action(self):
        act = np.random.choice(range(self.nb_action), 1)[0]
        return act

    def new_possible_random_action(self):
        possible_action = [i for i, x in enumerate(self.tr_matrix[self.current_state, :-1])
                           if x != self.current_state]
        possible_action.append(self.nb_action - 1)
        act = np.random.choice(possible_action, 1)[0]
        return act

    def new_w(self):
        state_prime = np.dot(self.sr_matrix[self.state_prime, :], self.w)
        state = np.dot(self.sr_matrix[self.state, :], self.w)
        # episodic learning
        if self.current_reward > 0.0:
            state_prime = 0.0

        td_update_w = self.current_reward + self.discount * state_prime - state
        feature_prev = self.sr_matrix[self.state, :]
        feature_prev_norm = np.dot(feature_prev, feature_prev)
        self.w += self.gamma_td * td_update_w * (feature_prev / feature_prev_norm)

    def set_initial_state(self, i, j):
        self.initial_state = self.ij_to_number(i, j)
        self.current_state = self.initial_state
        self.state = self.current_state

    """
    Create a rectangular gridworld where each cell (except the borders) are connected to
    its neighbours 
    """

    def create_tr_matrix(self):
        for i in range(self.height):
            for j in range(self.width):
                # stay
                self.tr_matrix[self.ij_to_number(i, j), 4] = self.ij_to_number(i, j)
                # up
                if i == 0:
                    self.tr_matrix[self.ij_to_number(i, j), 0] = self.ij_to_number(i, j)
                else:
                    self.tr_matrix[self.ij_to_number(i, j), 0] = self.ij_to_number(i - 1, j)
                # right
                if j == self.width - 1:
                    self.tr_matrix[self.ij_to_number(i, j), 3] = self.ij_to_number(i, j)
                else:
                    self.tr_matrix[self.ij_to_number(i, j), 3] = self.ij_to_number(i, j + 1)
                # left
                if j == 0:
                    self.tr_matrix[self.ij_to_number(i, j), 2] = self.ij_to_number(i, j)
                else:
                    self.tr_matrix[self.ij_to_number(i, j), 2] = self.ij_to_number(i, j - 1)
                # down
                if i == self.height - 1:
                    self.tr_matrix[self.ij_to_number(i, j), 1] = self.ij_to_number(i, j)
                else:
                    self.tr_matrix[self.ij_to_number(i, j), 1] = self.ij_to_number(i + 1, j)

    """
    Add the walls to the base gridworld rectangle 
    """

    def create_walls(self, filename):
        with open(filename, mode="r") as f_r:
            l_file = f_r.read().splitlines()
            if (len(l_file) - 1) % 2 != 0 or (len(l_file[0]) - 1) % 4 != 0:
                raise ValueError("Wrong structure")
            else:
                for i in range(0, len(l_file) - 1, 2):
                    for j in range(0, len(l_file[i]) - 1, 4):
                        if l_file[i][j] == " ":
                            continue
                        else:
                            i_p = i // 2
                            j_p = j // 4
                            # up
                            if l_file[i][j + 1] == "-":
                                self.tr_matrix[self.ij_to_number(i_p, j_p), 0] = \
                                    self.ij_to_number(i_p, j_p)
                            # right
                            if l_file[i + 1][j + 4] == "|":
                                self.tr_matrix[self.ij_to_number(i_p, j_p), 3] = \
                                    self.ij_to_number(i_p, j_p)
                            # left
                            if l_file[i + 1][j] == "|":
                                self.tr_matrix[self.ij_to_number(i_p, j_p), 2] = \
                                    self.ij_to_number(i_p, j_p)
                            # down
                            if l_file[i + 2][j + 1] == "-":
                                self.tr_matrix[self.ij_to_number(i_p, j_p), 1] = \
                                    self.ij_to_number(i_p, j_p)

    """
    modify the wall of the (i,j) position for a given action
    (status = add: true, remove: false)  

    """

    def modify_wall(self, i, j, action, status):
        if status:
            self.add_wall(i, j, action)
        else:
            self.remove_wall(i, j, action)

    """
    add the wall of the (i,j) position for a given action
    """

    def add_wall(self, i, j, action):
        rev_action = 0
        if action == 0:
            rev_action = 1
        if action == 1:
            rev_action = 0
        if action == 2:
            rev_action = 3
        if action == 3:
            rev_action = 2

        n1 = self.ij_to_number(i, j)
        n2 = self.tr_matrix[n1, action]
        if n1 != n2:
            self.tr_matrix[n1, action] = n1
            self.tr_matrix[n2, rev_action] = n2

    """
    remove the wall of the (i,j) position for a given action
    """

    def remove_wall(self, i, j, action):
        if action == 0 and i == 0 \
                or action == 3 and i == self.height - 1 \
                or action == 2 and j == 0 \
                or action == 1 and j == self.width - 1:
            return
        n1 = self.ij_to_number(i, j)
        if action == 0:
            # up
            n2 = self.ij_to_number(i - 1, j)
            self.tr_matrix[n1, 0] = n2
            self.tr_matrix[n2, 1] = n1

        elif action == 1:
            # down
            n2 = self.ij_to_number(i + 1, j)
            self.tr_matrix[n1, 1] = n2
            self.tr_matrix[n2, 0] = n1
        elif action == 2:
            # left
            n2 = self.ij_to_number(i + 1, j - 1)
            self.tr_matrix[n1, 2] = n2
            self.tr_matrix[n2, 3] = n1
        else:
            # right
            n2 = self.ij_to_number(i, j + 1)
            self.tr_matrix[n1, 3] = n2
            self.tr_matrix[n2, 2] = n1

    """
    (row, column) pair to cell number
    """

    def ij_to_number(self, i, j):
        return int(i * self.width + j)

    """
    cell number to (row,column) pair
    """

    def number_to_ij(self, n):
        return n // self.width, n % self.width

    def __str__(self):
        _str = ""
        return _str

    def set_new_reward(self, pos, value):
        i, j = pos
        n1 = self.ij_to_number(i, j)
        self.rewards[n1] = value

    def animate(self):
        if self.animate_flag:
            _str = ""
            for i in range(self.height):
                for k in range(2):
                    if k == 0:
                        for j in range(self.width):
                            if self.tr_matrix[self.ij_to_number(i, j), 0] == self.ij_to_number(i, j):
                                _str += "+---"
                            else:
                                _str += "+   "
                        _str += "\n"
                    else:
                        for j in range(self.width):
                            if self.tr_matrix[self.ij_to_number(i, j), 2] == self.ij_to_number(i, j):
                                _str += "|"
                            else:
                                _str += " "
                            if self.current_state == self.ij_to_number(i, j):
                                _str += " o "
                            else:
                                _str += "   "
                        _str += "\n"
            for j in range(self.width):
                if self.tr_matrix[self.ij_to_number(self.height - 1, j), 3] == self.ij_to_number(self.height - 1, j):
                    _str += "+---"
                else:
                    _str += "+   "
            _str += "\n"
            time.sleep(0.1)
            sys.stdout.write(_str)
            sys.stdout.flush()
            print("state = {}".format(self.state))
            print("state_prime = {}".format(self.state_prime))
            print('norm w_j = {}'.format(np.linalg.norm(self.w_j)))
            print(f'signal {self.signal}')
            print(f"reward = {self.current_reward}")
            print(utils.print_table("State distance", self.sr_state, self.width))
            print(utils.print_table("State distance prime", self.sr_state_prime, self.width))
            print(utils.print_table("Previous features", self.previous_features, self.width))
            print(utils.print_table("Features", self.features, self.width))
            print(utils.print_table("W_j", self.w_j, self.width))

