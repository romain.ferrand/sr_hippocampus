import maze_handler
import numpy as np
import math
import sys
import multiprocessing as mp


def quniform_float(low, high, step):
    high = high + sys.float_info.epsilon
    return int(math.ceil(np.random.uniform(low, high) / step)) * step


def quniform_int(low, high, step):
    high = high + 1
    return int(math.ceil(np.random.randint(low, high) / step)) * step


def gen_config_relearn_SR_TD():
    cfg = dict()
    sample_reward = quniform_int(10, 50, 10)
    sample_change_pos = quniform_int(10, 50, 10)

    cfg["simulation_data"] = {
        "simulation_name": "relearn_maze",
        "method_type": {
            "name": "SR-TD",
            "initial_parameters": {
                "gamma_sr": quniform_float(0.0, 1.0, 0.1),
                "gamma_td": quniform_float(0.0, 1.0, 0.1),
                "discount": quniform_float(0.0, 1.0, 0.1),
                "epsilon": quniform_float(0.0, 1.0, 0.1),
                "reward_pos": [8, 9],
                "start_pos": [[7, 0], [8, 8]]
            }
        },
        "maze": {
            "path": "mazes/latent_maze.txt",
            "width": 10,
            "height": 10
        },
        "exp_events": [
            {"run_phase": {
                "samples": 1,
                "max_step": quniform_int(5000, 50000, 5000),
                "initial_pos": [[7, 0]]
            }
            },
            {"reward_change": {
                "cell_pos": [1, 9],
                "value": 10
            }
            },
            {"run_phase": {
                "samples": sample_reward,
                "max_step": 1,
                "initial_pos": [[1, 9]]
            }
            },
            {"run_phase": {
                "samples": 1,
                "max_step": -1,
                "initial_pos": [[7, 0]]
            }
            },
            {"run_phase": {
                "samples": sample_change_pos,
                "max_step": -1,
                "initial_pos": [[7, 0], [8, 8]]
            }
            },
            {"reward_change": {
                "cell_pos": [8, 9],
                "value": 20
            }
            },
            {"run_phase": {
                "samples": sample_reward,
                "max_step": 1,
                "initial_pos": [[8, 9]]
            }
            }
        ],
        "animated": False,
        "seed": None
    }
    return cfg


def gen_config_relearn_SR_MB():
    cfg = dict()
    sample_reward = quniform_int(10, 50, 10)
    sample_change_pos = quniform_int(10, 50, 10)

    cfg["simulation_data"] = {
        "simulation_name": "relearn_maze",
        "method_type": {
            "name": "SR-MB",
            "initial_parameters": {
                "gamma_policy": quniform_float(0.0, 1.0, 0.1),
                "gamma_td": quniform_float(0.0, 1.0, 0.1),
                "discount": quniform_float(0.0, 1.0, 0.1),
                "epsilon": quniform_float(0.0, 1.0, 0.1),
                "reward_pos": [8, 9],
                "start_pos": [[7, 0], [8, 8]]
            }
        },
        "maze": {
            "path": "mazes/latent_maze.txt",
            "width": 10,
            "height": 10
        },
        "exp_events": [
            {"run_phase": {
                "samples": 1,
                "max_step": quniform_int(5000, 50000, 5000),
                "initial_pos": [[7, 0]]
            }
            },
            {"reward_change": {
                "cell_pos": [1, 9],
                "value": 10
            }
            },
            {"run_phase": {
                "samples": sample_reward,
                "max_step": 1,
                "initial_pos": [[1, 9]]
            }
            },
            {"run_phase": {
                "samples": 1,
                "max_step": -1,
                "initial_pos": [[7, 0]]
            }
            },
            {"run_phase": {
                "samples": sample_change_pos,
                "max_step": -1,
                "initial_pos": [[7, 0], [8, 8]]
            }
            },
            {"reward_change": {
                "cell_pos": [8, 9],
                "value": 20
            }
            },
            {"run_phase": {
                "samples": sample_reward,
                "max_step": 1,
                "initial_pos": [[8, 9]]
            }
            }
        ],
        "animated": False,
        "seed": None
    }
    return cfg


def gen_config_relearn_TD():
    cfg = dict()
    sample_reward = quniform_int(10, 50, 10)
    sample_change_pos = quniform_int(10, 50, 10)

    cfg["simulation_data"] = {
        "simulation_name": "relearn_maze",
        "method_type": {
            "name": "TD",
            "initial_parameters": {
                "gamma_e": quniform_float(0.0, 1.0, 0.1),
                "gamma_td": quniform_float(0.0, 1.0, 0.1),
                "discount": quniform_float(0.0, 1.0, 0.1),
                "epsilon": quniform_float(0.0, 1.0, 0.1),
                "reward_pos": [8, 9],
                "start_pos": [[7, 0], [8, 8]]
            }
        },
        "maze": {
            "path": "mazes/latent_maze.txt",
            "width": 10,
            "height": 10
        },
        "exp_events": [
            {"reward_change": {
                "cell_pos": [1, 9],
                "value": 10
            }
            },
            {"run_phase": {
                "samples": 1,
                "max_step": -1,
                "initial_pos": [[7, 0]]
            }
            },
            {"run_phase": {
                "samples": sample_change_pos,
                "max_step": -1,
                "initial_pos": [[7, 0], [8, 8]]
            }
            },
            {"reward_change": {
                "cell_pos": [8, 9],
                "value": 20
            }
            },
            {"run_phase": {
                "samples": sample_reward,
                "max_step": 1,
                "initial_pos": [[8, 9]]
            }
            }
        ],
        "animated": False,
        "seed": None
    }
    return cfg


def gen_config_latent_SR_TD():
    cfg = dict()
    sample_change_pos = quniform_int(10, 50, 10)

    cfg["simulation_data"] = {
        "simulation_name": "latent_maze",
        "method_type": {
            "name": "SR-TD",
            "initial_parameters": {
                "gamma_sr": quniform_float(0.0, 1.0, 0.1),
                "gamma_td": quniform_float(0.0, 1.0, 0.1),
                "discount": quniform_float(0.0, 1.0, 0.1),
                "epsilon": quniform_float(0.0, 1.0, 0.1),
                "reward_pos": [1, 9],
                "start_pos": [[7, 0]]
            }
        },
        "maze": {
            "path": "mazes/latent_maze.txt",
            "width": 10,
            "height": 10
        },
        "exp_events": [
            {"run_phase": {
                "samples": 1,
                "max_step": quniform_int(5000, 50000, 5000),
                "initial_pos": [[7, 0]]
            }
            },
            {"reward_change":
                {
                    "cell_pos": [1, 9],
                    "value": 10
                }
            },
            {"run_phase": {
                "samples": sample_change_pos,
                "max_step": 1,
                "initial_pos": [[1, 9]]
            }
            }
        ],
        "animated": False,
        "seed": None
    }
    return cfg


def gen_config_latent_SR_MB():
    cfg = dict()
    sample_change_pos = quniform_int(10, 50, 10)

    cfg["simulation_data"] = {
        "simulation_name": "latent_maze",
        "method_type": {
            "name": "SR-MB",
            "initial_parameters": {
                "gamma_policy": quniform_float(0.0, 1.0, 0.1),
                "gamma_td": quniform_float(0.0, 1.0, 0.1),
                "discount": quniform_float(0.0, 1.0, 0.1),
                "epsilon": quniform_float(0.0, 1.0, 0.1),
                "reward_pos": [1, 9],
                "start_pos": [[7, 0]]
            }
        },
        "maze": {
            "path": "mazes/latent_maze.txt",
            "width": 10,
            "height": 10
        },
        "exp_events": [
            {"run_phase": {
                "samples": 1,
                "max_step": quniform_int(5000, 50000, 5000),
                "initial_pos": [[7, 0]]
            }
            },
            {"reward_change":
                {
                    "cell_pos": [1, 9],
                    "value": 10
                }
            },
            {"run_phase": {
                "samples": sample_change_pos,
                "max_step": 1,
                "initial_pos": [[1, 9]]
            }
            }
        ],
        "animated": False,
        "seed": None
    }
    return cfg


def gen_config_latent_TD():
    cfg = dict()
    cfg["simulation_data"] = {
        "simulation_name": "latent_maze",
        "method_type": {
            "name": "TD",
            "initial_parameters": {
                "gamma_e": quniform_float(0.0, 1.0, 0.1),
                "gamma_td": quniform_float(0.0, 1.0, 0.1),
                "discount": quniform_float(0.0, 1.0, 0.1),
                "epsilon": quniform_float(0.0, 1.0, 0.1),
                "reward_pos": [1, 9],
                "start_pos": [[7, 0]]
            }
        },
        "maze": {
            "path": "mazes/latent_maze.txt",
            "width": 10,
            "height": 10
        },
        "exp_events": [
            {"reward_change":
                {
                    "cell_pos": [1, 9],
                    "value": 10
                }
            },
            {"run_phase": {
                "samples": 1,
                "max_step": -1,
                "initial_pos": [[7, 0]]
            }
            },
        ],
        "animated": False,
        "seed": None
    }
    return cfg


def gen_config_detour_SR_TD():
    wall_sample = quniform_int(10, 60, 10)
    reward_sample = quniform_int(10, 60, 10)
    relearn_sample = quniform_int(1, 20, 1)
    cfg = dict()
    cfg["simulation_data"] = {
        "simulation_name": "detour_maze",
        "method_type": {
            "name": "SR-TD",
            "initial_parameters": {
                "gamma_sr": quniform_float(0.0, 1.0, 0.1),
                "gamma_td": quniform_float(0.0, 1.0, 0.1),
                "discount": quniform_float(0.0, 1.0, 0.1),
                "epsilon": quniform_float(0.0, 1.0, 0.1),
                "reward_pos": [2, 9],
                "start_pos": [[2, 0]]
            }
        },
        "maze": {
            "path": "mazes/detour_maze.txt",
            "width": 10,
            "height": 10
        },
        "exp_events": [
            {"run_phase": {
                "samples": 1,
                "max_step": quniform_int(5000, 50000, 5000),
                "initial_pos": [[2, 0]]
            }
            },
            {"reward_change":
                {
                    "cell_pos": [2, 9],
                    "value": 10
                }
            },
            {"run_phase": {
                "samples": reward_sample,
                "max_step": 1,
                "initial_pos": [[2, 9]]
            }
            },
            {"run_phase": {
                "samples": relearn_sample,
                "max_step": -1,
                "initial_pos": [[2, 0]]
            }
            },
            {
                "wall_change": {
                    "cell_pos": [2, 5],
                    "wall_dir": 1,
                    "wall_status": True
                }
            },
            {
                "wall_change": {
                    "cell_pos": [2, 5],
                    "wall_dir": 2,
                    "wall_status": True
                }
            },
            {"run_phase":
                {
                    "samples": wall_sample,
                    "max_step": 1,
                    "initial_pos": [[2, 4]]
                }
            }
        ],
        "animated": False,
        "seed": None
    }
    return cfg


def gen_config_detour_TD():
    wall_sample = quniform_int(10, 60, 10)
    reward_sample = quniform_int(10, 60, 10)
    relearn_sample = quniform_int(1, 20, 1)
    cfg = dict()
    cfg["simulation_data"] = {
        "simulation_name": "detour_maze",
        "method_type": {
            "name": "SR-MB",
            "initial_parameters": {
                "gamma_policy": quniform_float(0.0, 1.0, 0.1),
                "gamma_td": quniform_float(0.0, 1.0, 0.1),
                "discount": quniform_float(0.0, 1.0, 0.1),
                "epsilon": quniform_float(0.0, 1.0, 0.1),
                "reward_pos": [2, 9],
                "start_pos": [[2, 0]]
            }
        },
        "maze": {
            "path": "mazes/detour_maze.txt",
            "width": 10,
            "height": 10
        },
        "exp_events": [
            {"reward_change":
                {
                    "cell_pos": [2, 9],
                    "value": 10
                }
            },
            {"run_phase": {
                "samples": reward_sample,
                "max_step": 1,
                "initial_pos": [[2, 9]]
            }
            },
            {"run_phase": {
                "samples": relearn_sample,
                "max_step": -1,
                "initial_pos": [[2, 0]]
            }
            },
            {
                "wall_change": {
                    "cell_pos": [2, 5],
                    "wall_dir": 1,
                    "wall_status": True
                }
            },
            {
                "wall_change": {
                    "cell_pos": [2, 5],
                    "wall_dir": 2,
                    "wall_status": True
                }
            },
            {"run_phase":
                {
                    "samples": wall_sample,
                    "max_step": 1,
                    "initial_pos": [[2, 4]]
                }
            }
        ],
        "animated": False,
        "seed": None
    }
    return cfg


def gen_config_detour_SR_MB():
    wall_sample = quniform_int(10, 60, 10)
    reward_sample = quniform_int(10, 60, 10)
    relearn_sample = quniform_int(1, 20, 1)
    cfg = dict()
    cfg["simulation_data"] = {
        "simulation_name": "detour_maze",
        "method_type": {
            "name": "SR-MB",
            "initial_parameters": {
                "gamma_policy": quniform_float(0.0, 1.0, 0.1),
                "gamma_td": quniform_float(0.0, 1.0, 0.1),
                "discount": quniform_float(0.0, 1.0, 0.1),
                "epsilon": quniform_float(0.0, 1.0, 0.1),
                "reward_pos": [2, 9],
                "start_pos": [[2, 0]]
            }
        },
        "maze": {
            "path": "mazes/detour_maze.txt",
            "width": 10,
            "height": 10
        },
        "exp_events": [
            {"run_phase": {
                "samples": 1,
                "max_step": quniform_int(5000, 50000, 5000),
                "initial_pos": [[2, 0]]
            }
            },
            {"reward_change":
                {
                    "cell_pos": [2, 9],
                    "value": 10
                }
            },
            {"run_phase": {
                "samples": reward_sample,
                "max_step": 1,
                "initial_pos": [[2, 9]]
            }
            },
            {"run_phase": {
                "samples": relearn_sample,
                "max_step": -1,
                "initial_pos": [[2, 0]]
            }
            },
            {
                "wall_change": {
                    "cell_pos": [2, 5],
                    "wall_dir": 1,
                    "wall_status": True
                }
            },
            {
                "wall_change": {
                    "cell_pos": [2, 5],
                    "wall_dir": 2,
                    "wall_status": True
                }
            },
            {"run_phase":
                {
                    "samples": wall_sample,
                    "max_step": 1,
                    "initial_pos": [[2, 4]]
                }
            }
        ],
        "animated": False,
        "seed": None
    }
    return cfg

def gen_config_detour_sample_wall_maze_SRTD_AC():
    discount = 1
    lr1 = 0.1
    lr2 = 0.5
    max_step = 10000
    cfg = dict()
    cfg["simulation_data"] = {
        "simulation_name": "simple_detour_SRTD_AC_sample_maze",
        "phase_params": ["sr_state"],
        "cycle_params": [],
        "method_type": {
            "name": "AC_SR-TD",
            "initial_parameters": {
                "nu": lr2,
                "alpha_sr": lr1,
                "lambda_sr": lr1,
                "discount": discount,
                "reward_pos": [2, 6],
                "start_pos": [[2, 0]]
            }
        },
        "maze": {
            "path": "mazes/simple_detour.txt",
            "width": 7,
            "height": 7
        },
        "exp_events": [
            {"run_phase": {
                "samples": 1,
                "max_step": max_step,
                "initial_pos": [[2, 0]],
                "sample_maze": True,
                "output": False
            }
            },
            {"reward_change":
                {
                    "cell_pos": [2, 6],
                    "value": 10
                }
            },
            {"run_phase": {
                "samples": 20,
                "max_step": -1,
                "initial_pos": [[2, 0]],
                "sample_maze": False,
                "output": False

            }
            },
            {"wall_change": {
                "cell_pos": [2, 2],
                "wall_dir": 3,
                "wall_status": True
            }
            },
            {"run_phase":
                {
                    "samples": 50,
                    "max_step": 1,
                    "initial_pos": [[2, 2]],
                    "sample_maze": False,
                    "output": True
                }
            }
        ],
        "animated": False,
        "seed": None
    }
    return cfg

def gen_config_detour_AC_TD_lambda():
    lr2 = 0.5
    ld = 0.1
    discount = 0.9
    max_step = 10000
    cfg = dict()
    cfg["simulation_data"] = {
        "simulation_name": "detour_2",
        "phase_params": ["max_step"],
        "cycle_params": [],
        "method_type": {
            "name": "AC",
            "initial_parameters": {
                "nu": lr2,
                "ld": ld,
                "discount": discount,
                "reward_pos": [2, 6],
                "start_pos": [[2, 0]]
            }
        },
        "maze": {
            "path": "mazes/simple_detour.txt",
            "width": 7,
            "height": 7
        },
        "exp_events": [
            {"run_phase": {
                "samples": 1,
                "max_step": max_step,
                "initial_pos": [[2, 0]],
                "sample_maze": True,
                "output": False
            }
            },
            {"reward_change":
                {
                    "cell_pos": [2, 6],
                    "value": 10
                }
            },
            {"run_phase": {
                "samples": 20,
                "max_step": -1,
                "initial_pos": [[2, 0]],
                "sample_maze": False,
                "output": True

            }
            },
            {"wall_change": {
                "cell_pos": [2, 2],
                "wall_dir": 3,
                "wall_status": True
            }
            },
            {"run_phase":
                {
                    "samples": 20,
                    "max_step": -1,
                    "initial_pos": [[2, 0]],
                    "sample_maze": False,
                    "output": True
                }
            }
        ],
        "animated": False,
        "seed": None
    }
    return cfg

def gen_config_detour_maze_SRTD_AC():
    discount = 1
    lr1 = 0.1
    lr2 = 0.5
    max_step = 10000
    cfg = dict()
    cfg["simulation_data"] = {
        "simulation_name": "simple_detour_SRTD_AC_v2",
        "phase_params": ["w_j", "max_step"],
        "cycle_params": [],
        "method_type": {
            "name": "AC_SR-TD",
            "initial_parameters": {
                "nu": lr2,
                "alpha_sr": lr1,
                "lambda_sr": lr1,
                "discount": discount,
                "reward_pos": [2, 6],
                "start_pos": [[2, 0]]
            }
        },
        "maze": {
            "path": "mazes/simple_detour.txt",
            "width": 7,
            "height": 7
        },
        "exp_events": [
            {"run_phase": {
                "samples": 1,
                "max_step": max_step,
                "initial_pos": [[2, 0]],
                "sample_maze": True,
                "output": False
            }
            },
            {"reward_change":
                {
                    "cell_pos": [2, 6],
                    "value": 10
                }
            },
            {"run_phase": {
                "samples": 20,
                "max_step": -1,
                "initial_pos": [[2, 0]],
                "sample_maze": False,
                "output": True

            }
            },
            {"wall_change": {
                "cell_pos": [2, 2],
                "wall_dir": 3,
                "wall_status": True
            }
            },
            {"run_phase":
                {
                    "samples": 20,
                    "max_step": -1,
                    "initial_pos": [[2, 0]],
                    "sample_maze": False,
                    "output": True
                }
            }
        ],
        "animated": False,
        "seed": None
    }
    return cfg


def gen_config_detour_maze_SRTD_distance():
    lr1 = 0.1
    lr2 = 0.5
    t_star_min = 0.08
    c = 0.08
    k = 8
    nb_windows = 100
    max_step = 5000
    discount = 1
    cfg = dict()
    cfg["simulation_data"] = {
        "simulation_name": "simple_detour_maze_SRTD_multi-scale_V2",
        "phase_params": ["state_dist", "w_j", "max_step"],
        "cycle_params": [],
        "method_type": {
            "name": "AC_SR-TD_distance",
            "initial_parameters": {
                "epsilon": 0.0,
                "alpha_sr": lr1,
                "lambda_sr": lr1,
                "mu": lr2,
                "nu": lr2,
                "discount": discount,
                "reward_pos": [2, 6],
                "start_pos": [[2, 0]],
                "t_star_min": t_star_min,
                "c": c,
                "k": k,
                "nb_windows": nb_windows
            }
        },
        "maze": {
            "path": "mazes/simple_detour.txt",
            "width": 7,
            "height": 7
        },
        "exp_events": [
            {"run_phase": {
                "samples": 1,
                "max_step": max_step,
                "initial_pos": [[2, 0]],
                "sample_maze": True,
                "output": False
            }
            },
            {"reward_change":
                {
                    "cell_pos": [2, 6],
                    "value": 10
                }
            },
            {"run_phase": {
                "samples": 20,
                "max_step": -1,
                "initial_pos": [[2, 0]],
                "sample_maze": False,
                "output": True

            }
            },
            {"wall_change": {
                "cell_pos": [2, 2],
                "wall_dir": 3,
                "wall_status": True
            }
            },
            {"run_phase":
                {
                    "samples": 20,
                    "max_step": -1,
                    "initial_pos": [[2, 0]],
                    "sample_maze": False,
                    "output": True
                }
            }
        ],
        "animated": True,
        "seed": None
    }
    return cfg

def gen_config_detour_maze_SRTD_distance_wall_sample():
    lr1 = 0.1
    lr2 = 0.5
    t_star_min = 0.08
    c = 0.08
    k = 2
    nb_windows = 100
    max_step = 10000
    discount = 1
    cfg = dict()
    cfg["simulation_data"] = {
        "simulation_name": "simple_detour_maze_SRTD_multi-scale_wall_sample",
        "phase_params": ["w_j", "distance"],
        "cycle_params": [],
        "method_type": {
            "name": "AC_SR-TD_distance",
            "initial_parameters": {
                "epsilon": 0.0,
                "alpha_sr": lr1,
                "lambda_sr": lr1,
                "mu": lr2,
                "nu": lr2,
                "discount": discount,
                "reward_pos": [2, 6],
                "start_pos": [[2, 0]],
                "t_star_min": t_star_min,
                "c": c,
                "k": k,
                "nb_windows": nb_windows
            }
        },
        "maze": {
            "path": "mazes/simple_detour.txt",
            "width": 7,
            "height": 7
        },
        "exp_events": [
            {"run_phase": {
                "samples": 1,
                "max_step": max_step,
                "initial_pos": [[2, 0]],
                "sample_maze": True,
                "output": False
            }
            },
            {"reward_change":
                {
                    "cell_pos": [2, 6],
                    "value": 10
                }
            },
            {"run_phase": {
                "samples": 20,
                "max_step": -1,
                "initial_pos": [[2, 0]],
                "sample_maze": False,
                "output": False

            }
            },
            {"wall_change": {
                "cell_pos": [2, 2],
                "wall_dir": 3,
                "wall_status": True
            }
            },
            {"run_phase":
                {
                    "samples": 50,
                    "max_step": 1,
                    "initial_pos": [[2, 2]],
                    "sample_maze": False,
                    "output": True
                }
            }
        ],
        "animated": True,
        "seed": None
    }
    return cfg

def gen_config_simple_relearn_maze_SRTD_distance():
    lr1 = 0.1
    lr2 = 0.5
    t_star_min = 0.08
    c = 0.08
    k = 8
    nb_windows = 100
    discount = 1
    max_step = 10000
    cfg = dict()
    cfg["simulation_data"] = {
        "simulation_name": "simple_relearn_maze_SRTD_distance_multi-scale",
        "phase_params": ["state_dist",
                         "w_j",
                         "max_step",
                         "max_reward"],
        "cycle_params": [],
        "method_type": {
            "name": "AC_SR-TD_distance",
            "initial_parameters": {
                "epsilon" : 0.0,
                "alpha_sr": lr1,
                "lambda_sr": lr1,
                "mu": lr2,
                "nu": lr2,
                "discount": discount,
                "reward_pos": [6, 6],
                "start_pos": [[1, 0]],
                "t_star_min": t_star_min,
                "c": c,
                "k": k,
                "nb_windows": nb_windows
            }
        },
        "maze": {
            "path": "mazes/simple_latent_maze.txt",
            "width": 7,
            "height": 7
        },
        "exp_events": [
            {"run_phase": {
                "samples": 1,
                "max_step": max_step,
                "initial_pos": [[1, 0]],
                "sample_maze": True,
                "output": False
            }
            },
            {"reward_change":
                {
                    "cell_pos": [0, 6],
                    "value": 10
                }
            },
            {"run_phase": {
                "samples": 10,
                "max_step": 1,
                "initial_pos": [[0, 6]],
                "sample_maze": False,
                "output": False
            }
            },
            {"run_phase": {
                "samples": 20,
                "max_step": -1,
                "initial_pos": [[1, 0]],
                "sample_maze": False,
                "output": True
            }
            },
            {"reward_change":
                {
                    "cell_pos": [6, 6],
                    "value": 100
                }
            },
            {"run_phase": {
                "samples": 10,
                "max_step": 1,
                "initial_pos": [[6, 6]],
                "sample_maze": False,
                "output": False
            }
            },
            {"run_phase":
                {
                    "samples": 20,
                    "max_step": -1,
                    "initial_pos": [[1, 0]],
                    "sample_maze": False,
                    "output": True
                }
            }
        ],
        "animated": True,
        "seed": None
    }
    return cfg


def gen_config_simple_relearn_maze_SRTD_AC():
    discount = 0.9
    lr1 = 0.1
    lr2 = 0.1
    max_step = 10000
    cfg = dict()
    cfg["simulation_data"] = {
        "simulation_name": "simple_relearn_SRTD_AC",
        "phase_params": ["w_j", "max_step", "max_reward"],
        "cycle_params": [],
        "method_type": {
            "name": "AC_SR-TD",
            "initial_parameters": {
                "nu": lr2,
                "alpha_sr": lr1,
                "lambda_sr": lr1,
                "discount": discount,
                "reward_pos": [6, 6],
                "start_pos": [[1, 0]]
            }
        },
        "maze": {
            "path": "mazes/simple_latent_maze.txt",
            "width": 7,
            "height": 7
        },
        "exp_events": [
            {"run_phase": {
                "samples": 1,
                "max_step": max_step,
                "initial_pos": [[1, 0]],
                "sample_maze": True,
                "output": False
            }
            },
            {"reward_change":
                {
                    "cell_pos": [0, 6],
                    "value": 10
                }
            },
            {"run_phase": {
                "samples": 10,
                "max_step": 1,
                "initial_pos": [[0, 6]],
                "sample_maze": False,
                "output": False
            }
            },
            {"run_phase": {
                "samples": 20,
                "max_step": -1,
                "initial_pos": [[1, 0]],
                "sample_maze": False,
                "output": True
            }
            },
            {"reward_change":
                {
                    "cell_pos": [6, 6],
                    "value": 100
                }
            },
            {"run_phase": {
                "samples": 10,
                "max_step": 1,
                "initial_pos": [[6, 6]],
                "sample_maze": False,
                "output": False
            }
            },
            {"run_phase":
                {
                    "samples": 20,
                    "max_step": -1,
                    "initial_pos": [[1, 0]],
                    "sample_maze": False,
                    "output": True
                }
            }
        ],
        "animated": False,
        "seed": None
    }
    return cfg


def gen_config_latent_maze_AC_goal_directed_2goal():
    lr1 = 0.1
    lr2 = 0.5
    t_star_min = 0.08
    c = 0.08
    k = 4
    nb_windows = 100
    discount = 1
    max_step = 10000
    cfg = dict()
    cfg["simulation_data"] = {
            "simulation_name": "goal_directed_SRTD_distance_2goal",
        "phase_params": ["max_step"],
        "cycle_params": [],
        "method_type": {
            "name": "maze_AC_goal_directed",
            "initial_parameters": {
                "epsilon": 0.0,
                "alpha_sr": lr1,
                "lambda_sr": lr1,
                "mu": lr2,
                "nu": lr2,
                "discount": discount,
                "t_star_min": t_star_min,
                "c": c,
                "k": k,
                "nb_windows": nb_windows,
                "reward_pos": [6, 6],
                "wanted_feature": [0, 0, 0, 0, 0, 0, 0,
                                   0, 0, 0, 0, 0, 0, 0,
                                   0, 0, 0, 0, 0, 0, 0,
                                   0, 0, 0, 0, 0, 0, 0,
                                   0, 0, 0, 0, 0, 0, 0,
                                   0, 0, 0, 0, 0, 0, 0,
                                   0, 0, 0, 0, 0, 0, 1],
                "start_pos": [[1, 0]]
            }
        },
        "maze": {
            "path": "mazes/simple_latent_maze.txt",
            "width": 7,
            "height": 7
        },
        "exp_events": [
            {"run_phase": {
                "samples": 1,
                "max_step": max_step,
                "initial_pos": [[1, 0]],
                "sample_maze": True,
                "output": False
            }
            },
            {"run_phase":
                {
                    "samples": 20,
                    "max_step": -1,
                    "initial_pos": [[1, 0]],
                    "sample_maze": False,
                    "output": True
                }
            },
            {"params_change":
                {
                    "w_j": [0, 0, 0, 0, 0, 0, 1,
                            0, 0, 0, 0, 0, 0, 0,
                            0, 0, 0, 0, 0, 0, 0,
                            0, 0, 0, 0, 0, 0, 0,
                            0, 0, 0, 0, 0, 0, 0,
                            0, 0, 0, 0, 0, 0, 0,
                            0, 0, 0, 0, 0, 0, 0]
                }},
            {"run_phase":
                {
                    "samples": 20,
                    "max_step": -1,
                    "initial_pos": [[1, 0]],
                    "sample_maze": False,
                    "output": True
                }
            },
        ],
        "animated": True,
        "seed": None
    }
    return cfg

def gen_config_latent_maze_AC_goal_directed():
    lr1 = 0.1
    lr2 = 0.5
    t_star_min = 0.08
    c = 0.08
    k = 4
    nb_windows = 100
    discount = 1
    max_step = 10000
    cfg = dict()
    cfg["simulation_data"] = {
        "simulation_name": "goal_directed",
        "phase_params": ["max_step"],
        "cycle_params": [],
        "method_type": {
            "name": "maze_AC_goal_directed",
            "initial_parameters": {
                "epsilon": 0.0,
                "alpha_sr": lr1,
                "lambda_sr": lr1,
                "mu": lr2,
                "nu": lr2,
                "discount": discount,
                "t_star_min": t_star_min,
                "c": c,
                "k": k,
                "nb_windows": nb_windows,
                "reward_pos": [6, 6],
                "wanted_feature": [0, 0, 0, 0, 0, 0, 0,
                                   0, 0, 0, 0, 0, 0, 0,
                                   0, 0, 0, 0, 0, 0, 0,
                                   0, 0, 0, 0, 0, 0, 0,
                                   0, 0, 0, 0, 0, 0, 0,
                                   0, 0, 0, 0, 0, 0, 0,
                                   0, 0, 0, 0, 0, 0, 1],
                "start_pos": [[1, 0]]
            }
        },
        "maze": {
            "path": "mazes/simple_latent_maze.txt",
            "width": 7,
            "height": 7
        },
        "exp_events": [
            {"run_phase": {
                "samples": 1,
                "max_step": max_step,
                "initial_pos": [[1, 0]],
                "sample_maze": True,
                "output": False
            }
            },
            {"run_phase":
                {
                    "samples": 20,
                    "max_step": -1,
                    "initial_pos": [[1, 0]],
                    "sample_maze": False,
                    "output": True
                }
            },

        ],
        "animated": False,
        "seed": None
    }
    return cfg


def gen_config_latent_maze_AC_SR_goal_directed():
    lr1 = 0.1
    lr2 = 0.5
    discount = 0.9
    max_step = 10000
    cfg = dict()
    cfg["simulation_data"] = {
        "simulation_name": "goal_directed_SRTD",
        "phase_params": ["max_step"],
        "cycle_params": [],
        "method_type": {
            "name": "maze_AC_SR_goal_directed",
            "initial_parameters": {
                "epsilon": 0.0,
                "alpha_sr": lr1,
                "lambda_sr": lr1,
                "mu": lr2,
                "nu": lr2,
                "discount": discount,
                "reward_pos": [6, 6],
                "wanted_feature": [0, 0, 0, 0, 0, 0, 0,
                                   0, 0, 0, 0, 0, 0, 0,
                                   0, 0, 0, 0, 0, 0, 0,
                                   0, 0, 0, 0, 0, 0, 0,
                                   0, 0, 0, 0, 0, 0, 0,
                                   0, 0, 0, 0, 0, 0, 0,
                                   0, 0, 0, 0, 0, 0, 1],
                "start_pos": [[1, 0]]
            }
        },
        "maze": {
            "path": "mazes/simple_latent_maze.txt",
            "width": 7,
            "height": 7
        },
        "exp_events": [
            {"run_phase": {
                "samples": 1,
                "max_step": max_step,
                "initial_pos": [[1, 0]],
                "sample_maze": True,
                "output": False
            }
            },
            {"run_phase":
                {
                    "samples": 20,
                    "max_step": -1,
                    "initial_pos": [[1, 0]],
                    "sample_maze": False,
                    "output": True
                }
            }
        ],
        "animated": False,
        "seed": None
    }
    return cfg

def gen_config_latent_maze_AC_TD_lambda_goal_directed():
    lr2 = 0.5
    ld = 0.1
    discount = 0.9
    max_step = 10000
    cfg = dict()
    cfg["simulation_data"] = {
        "simulation_name": "goal_directed_SRTD",
        "phase_params": ["max_step"],
        "cycle_params": [],
        "method_type": {
            "name": "AC",
            "initial_parameters": {
                "nu": lr2,
                "ld": ld,
                "discount": discount,
                "reward_pos": [6, 6],
                "start_pos": [[1, 0]]
            }
        },
        "maze": {
            "path": "mazes/simple_latent_maze.txt",
            "width": 7,
            "height": 7
        },
        "exp_events": [
            {"run_phase": {
                "samples": 1,
                "max_step": max_step,
                "initial_pos": [[1, 0]],
                "sample_maze": True,
                "output": False
            }
            },
            {"reward_change":
                {
                    "cell_pos": [6, 6],
                    "value": 10
                }
            },
            {"run_phase":
                {
                    "samples": 20,
                    "max_step": -1,
                    "initial_pos": [[1, 0]],
                    "sample_maze": False,
                    "output": True
                }
            }
        ],
        "animated": False,
        "seed": None
    }
    return cfg


def gen_config_latent_maze_AC_TD_lambda_2_goal_directed():
    lr2 = 0.5
    ld = 0.1
    discount = 0.9
    max_step = 10000
    cfg = dict()
    cfg["simulation_data"] = {
        "simulation_name": "goal_directed_2_goal",
        "phase_params": ["max_step"],
        "cycle_params": [],
        "method_type": {
            "name": "AC",
            "initial_parameters": {
                "nu": lr2,
                "ld": ld,
                "discount": discount,
                "reward_pos": [6, 6],
                "start_pos": [[1, 0]]
            }
        },
        "maze": {
            "path": "mazes/simple_latent_maze.txt",
            "width": 7,
            "height": 7
        },
        "exp_events": [
            {"run_phase": {
                "samples": 1,
                "max_step": max_step,
                "initial_pos": [[1, 0]],
                "sample_maze": True,
                "output": False
            }
            },
            {"reward_change":
                {
                    "cell_pos": [6, 6],
                    "value": 10
                }
            },
            {"run_phase":
                {
                    "samples": 20,
                    "max_step": -1,
                    "initial_pos": [[1, 0]],
                    "sample_maze": False,
                    "output": True
                }
            },
            {"reward_change":
                {
                    "cell_pos": [6, 6],
                    "value": 0
                }
            },
            {"reward_change":
                {
                    "cell_pos": [0, 6],
                    "value": 10
                }
            },
            {"run_phase":
                {
                    "samples": 20,
                    "max_step": -1,
                    "initial_pos": [[1, 0]],
                    "sample_maze": False,
                    "output": True
                }
            },
        ],
        "animated": False,
        "seed": None
    }
    return cfg

def gen_config_latent_maze_AC_SR_goal_directed_2goal():
    lr1 = 0.1
    lr2 = 0.5
    discount = 0.9
    max_step = 10000
    cfg = dict()
    cfg["simulation_data"] = {
        "simulation_name": "goal_directed_SRTD_2goal",
        "phase_params": ["max_step"],
        "cycle_params": [],
        "method_type": {
            "name": "maze_AC_SR_goal_directed",
            "initial_parameters": {
                "epsilon": 0.0,
                "alpha_sr": lr1,
                "lambda_sr": lr1,
                "mu": lr2,
                "nu": lr2,
                "discount": discount,
                "reward_pos": [6, 6],
                "wanted_feature": [0, 0, 0, 0, 0, 0, 0,
                                   0, 0, 0, 0, 0, 0, 0,
                                   0, 0, 0, 0, 0, 0, 0,
                                   0, 0, 0, 0, 0, 0, 0,
                                   0, 0, 0, 0, 0, 0, 0,
                                   0, 0, 0, 0, 0, 0, 0,
                                   0, 0, 0, 0, 0, 0, 1],
                "start_pos": [[1, 0]]
            }
        },
        "maze": {
            "path": "mazes/simple_latent_maze.txt",
            "width": 7,
            "height": 7
        },
        "exp_events": [
            {"run_phase": {
                "samples": 1,
                "max_step": max_step,
                "initial_pos": [[1, 0]],
                "sample_maze": True,
                "output": False
            }
            },
            {"run_phase":
                {
                    "samples": 20,
                    "max_step": -1,
                    "initial_pos": [[1, 0]],
                    "sample_maze": False,
                    "output": True
                }
            },
            {"params_change":
                 {
                     "w_j": [0, 0, 0, 0, 0, 0, 1,
                            0, 0, 0, 0, 0, 0, 0,
                            0, 0, 0, 0, 0, 0, 0,
                            0, 0, 0, 0, 0, 0, 0,
                            0, 0, 0, 0, 0, 0, 0,
                            0, 0, 0, 0, 0, 0, 0,
                            0, 0, 0, 0, 0, 0, 0]
                 }},
            {"run_phase":
                {
                    "samples": 20,
                    "max_step": -1,
                    "initial_pos": [[1, 0]],
                    "sample_maze": False,
                    "output": True
                }
            },
        ],
        "animated": False,
        "seed": None
    }
    return cfg

def gen_config_cluster_maze_SR_distance():
    lr1 = 0.1
    lr2 = 0.1
    t_star_min = 0.08
    c = 0.08
    k = 8
    nb_windows = 100
    discount = 1
    max_step = 50000
    cfg = dict()
    cfg["simulation_data"] = {
        "simulation_name": "cluster_maze_SR_distance",
        "phase_params": ["distance"],
        "cycle_params": [],
        "method_type": {
            "name": "AC_SR-TD_distance",
            "initial_parameters": {
                "epsilon" : 0.0,
                "alpha_sr": lr1,
                "lambda_sr": lr1,
                "mu": lr2,
                "nu": lr2,
                "discount": discount,
                "reward_pos": [6, 6],
                "start_pos": [[1, 0]],
                "t_star_min": t_star_min,
                "c": c,
                "k": k,
                "nb_windows": nb_windows
            }
        },
        "maze": {
            "path": "mazes/cluster_maze.txt",
            "width": 5,
            "height": 6
        },
        "exp_events": [
            {"run_phase": {
                "samples": 1,
                "max_step": max_step,
                "initial_pos": [[0, 0]],
                "sample_maze": True,
                "output": False
            }
            },
            {"run_phase": {
                "samples": 1,
                "max_step": 1,
                "initial_pos": [[0, 0]],
                "sample_maze": False,
                "output": True
            }
            }
        ],
        "animated": True,
        "seed": None
    }
    return cfg

def gen_config_cluster_maze_SR_distance_5_rooms():
    lr1 = 0.1
    lr2 = 0.1
    t_star_min = 0.08
    c = 0.08
    k = 8
    nb_windows = 100
    discount = 1
    max_step = 50000
    cfg = dict()
    cfg["simulation_data"] = {
        "simulation_name": "cluster_maze_SR_distance_5_rooms",
        "phase_params": ["distance"],
        "cycle_params": [],
        "method_type": {
            "name": "AC_SR-TD_distance",
            "initial_parameters": {
                "epsilon" : 0.0,
                "alpha_sr": lr1,
                "lambda_sr": lr1,
                "mu": lr2,
                "nu": lr2,
                "discount": discount,
                "reward_pos": None,
                "start_pos": [[0, 0]],
                "t_star_min": t_star_min,
                "c": c,
                "k": k,
                "nb_windows": nb_windows
            }
        },
        "maze": {
            "path": "mazes/cluster_maze_5_rooms.txt",
            "width": 10,
            "height": 6
        },
        "exp_events": [
            {"run_phase": {
                "samples": 1,
                "max_step": max_step,
                "initial_pos": [[0, 0]],
                "sample_maze": True,
                "output": False
            }
            },
            {"run_phase": {
                "samples": 1,
                "max_step": 1,
                "initial_pos": [[0, 0]],
                "sample_maze": False,
                "output": True
            }
            }
        ],
        "animated": True,
        "seed": None
    }
    return cfg

def gen_config_cluster_maze_SR_distance_latent():
    lr1 = 0.1
    lr2 = 0.1
    t_star_min = 0.08
    c = 0.08
    k = 8
    nb_windows = 100
    discount = 1
    max_step = 50000
    cfg = dict()
    cfg["simulation_data"] = {
        "simulation_name": "cluster_maze_SR_distance_latent",
        "phase_params": ["distance"],
        "cycle_params": [],
        "method_type": {
            "name": "AC_SR-TD_distance",
            "initial_parameters": {
                "epsilon" : 0.0,
                "alpha_sr": lr1,
                "lambda_sr": lr1,
                "mu": lr2,
                "nu": lr2,
                "discount": discount,
                "reward_pos": None,
                "start_pos": [[1, 0]],
                "t_star_min": t_star_min,
                "c": c,
                "k": k,
                "nb_windows": nb_windows
            }
        },
        "maze": {
            "path": "mazes/latent_maze.txt",
            "width": 10,
            "height": 10
        },
        "exp_events": [
            {"run_phase": {
                "samples": 1,
                "max_step": max_step,
                "initial_pos": [[1, 0]],
                "sample_maze": True,
                "output": False
            }
            },
            {"run_phase": {
                "samples": 1,
                "max_step": 1,
                "initial_pos": [[1, 0]],
                "sample_maze": False,
                "output": True
            }
            }
        ],
        "animated": True,
        "seed": None
    }
    return cfg

def new_config():
    while True:
        cfgs = [
            #gen_config_detour_AC_TD_lambda(),
            #gen_config_latent_maze_AC_TD_lambda_goal_directed()
            #gen_config_latent_maze_AC_SR_goal_directed_2goal()
             #gen_config_latent_maze_AC_goal_directed_2goal(),
            #gen_config_latent_maze_AC_goal_directed()
             #gen_config_latent_maze_AC_SR_goal_directed()
            gen_config_detour_maze_SRTD_distance_wall_sample()
            #gen_config_detour_sample_wall_maze_SRTD_AC(),
            #gen_config_cluster_maze_SR_distance_latent()
            #gen_config_detour_maze_SRTD_distance(),
            # gen_config_detour_maze_SRTD_AC()
            # gen_config_cluster_maze_SR_distance()
            #gen_config_cluster_maze_SR_distance_5_rooms()

        ]
        i = np.random.randint(len(cfgs))
        yield cfgs[i]


if __name__ == "__main__":
    pool = mp.Pool(mp.cpu_count())
    results = []
    try:
        [pool.apply_async(maze_handler.MazeHandler(f), args=f) for f in new_config()]
    except KeyboardInterrupt:
        pool.close()
        pool.join()
        print("bye")
