from maze_actor_critic import MazeActorCritic
import numpy as np
import utils
import sys
import numpy

numpy.set_printoptions(threshold=sys.maxsize)


class MazeActorCriticSRTDWindows(MazeActorCritic):

    def __init__(self, maze_file):
        super().__init__(maze_file)
        self.nu = 0.0
        self.nb_windows = 9
        self.discount_sr = np.linspace(start=0, stop=1, num=100, endpoint=False)
        self.nb_windows = len(self.discount_sr)
        self.trace_features = np.zeros((self.nb_windows, self.nb_cells))
        self.w_ij = np.zeros((self.nb_action, self.nb_cells))
        self.w_ij_sr = np.tile(np.identity(self.nb_cells, float), (self.nb_windows, 1)).reshape((
            self.nb_windows, self.nb_cells, self.nb_cells))
        self.sr_state = np.zeros((self.nb_windows, self.nb_cells))
        self.sr_state_prime = np.zeros((self.nb_windows, self.nb_cells))
        self.alpha_sr = 0.0
        self.lambda_sr = 0.0
        self.mu = 0.0
        self.sigma = 0.0
        self.w_j = np.zeros((self.nb_windows, self.nb_cells,))
        self.w_o_j = np.zeros((self.nb_windows, self.nb_cells,))
        self.corr = np.repeat(1 / self.nb_windows, (self.nb_windows,))
        self.h = np.repeat(1 / self.nb_windows, (self.nb_windows,))
        self.o = np.repeat(1 / self.nb_windows, (self.nb_windows,))
        self.cred = np.repeat(1 / self.nb_windows, (self.nb_windows,))
        self.prev_cred = np.repeat(1 / self.nb_windows, (self.nb_windows,))
        self.diff = np.repeat(1 / self.nb_windows, (self.nb_windows,))
        self.previous_features = np.zeros((self.nb_cells,))
        self.features = np.zeros((self.nb_cells,))
        self.current_state = self.initial_state
        self.prev_state = self.initial_state
        self.prev_action = np.zeros((self.nb_action,))
        self.signals = np.zeros(self.nb_windows, )
        self.global_signal = 0.0
        self.global_state = 0.0
        self.global_state_prime = 0.0
        self.global_sr_state = 0.0
        self.global_sr_state_prime = 0.0

        self.state = np.zeros((self.nb_windows,))
        self.state_prime = np.zeros((self.nb_windows,))

    def new_sr_state(self):
        return np.dot(np.transpose(self.w_ij_sr, (0, 2, 1)), self.features)

    def new_global_signal(self):
        self.global_signal = np.dot(self.cred, self.signals)

    def new_signals(self):
        self.signals = self.current_reward \
                       + self.discount_sr * self.state_prime - self.state

    def set_params(self, params):
        self.nu = params["nu"]
        self.mu = params["mu"]
        self.sigma = params["sigma"]
        self.alpha_sr = params["alpha_sr"]
        self.lambda_sr = params["lambda_sr"]

    def new_global_state(self, state):
        return np.dot(self.cred, state)

    def new_global_sr_state(self, sr_state):
        g_sr_state = sr_state * self.cred[..., np.newaxis]
        g_sr_state = np.sum(g_sr_state, axis=0)
        return g_sr_state

    def update_sr(self):
        for i in range(self.nb_windows):
            self.trace_features[i] = self.discount_sr[i] * self.lambda_sr * \
                                     self.trace_features[i] + self.previous_features
            update_sr_wij = self.previous_features + self.discount_sr[i] * self.sr_state_prime[i] - self.sr_state[i]
            self.w_ij_sr[i] += self.alpha_sr * self.previous_features[..., np.newaxis] * update_sr_wij

    def update_actor(self):
        update = self.nu * self.global_signal * self.previous_features
        self.w_ij[self.action] += update

    def update_critic(self):
        for i in range(self.nb_windows):
            total = np.linalg.norm(self.sr_state[i])
            norm = self.previous_features if total == 0 else self.sr_state[i] / total
            self.w_j[i] += self.nu * self.signals[i] * norm

    def update_o(self):
        self.o = np.dot(self.w_o_j, self.previous_features)

    def update_credibility(self):
        self.prev_cred = self.cred
        self.cred = self.o / np.sum(self.o)
        # print("cred {}".format(self.cred))

    def update_corr(self):
        # print("signals = {}".format(self.signals))
        self.corr = np.exp(-0.5 * (self.signals / self.sigma) ** 2)
        # print("corr {}".format(self.corr))

    def new_h(self):
        _div = np.dot(self.prev_cred, self.corr)
        _div = 1 if _div == 0 else _div

        self.h = (self.prev_cred * self.corr) / _div
        # print("h {}".format(self.h))

    def new_diff(self):
        self.diff = self.h - self.cred
        # print("diff {}".format(self.diff))

    def update_w_o_j(self):
        for i in range(self.nb_windows):
            self.w_o_j[i] += self.mu * self.signals[i] * self.diff[i] * self.previous_features

    def v_value(self, sr_state):
        v_value = np.sum(sr_state * self.w_j, axis=1)
        # print("v_value {}".format(v_value))
        return v_value

    def reset_maze(self):
        super(MazeActorCriticSRTDWindows, self).reset_maze()

        self.state = np.zeros((self.nb_windows,))
        self.state_prime = np.zeros((self.nb_windows,))
        self.features = np.zeros((self.nb_cells,))
        self.previous_features = np.zeros((self.nb_cells,))
        self.signals = np.zeros(self.nb_windows, )
        self.sr_state = np.zeros((self.nb_windows, self.nb_cells))
        self.sr_state_prime = np.zeros((self.nb_windows, self.nb_cells))

        self.global_signal = 0.0
        self.global_state = 0.0
        self.global_state_prime = 0.0
        self.global_sr_state = 0.0
        self.global_sr_state_prime = 0.0

    def update(self, sample_maze):

        self.update_sr()
        if not sample_maze:
            self.update_actor()
            self.update_critic()
            self.update_o()
            self.update_credibility()
            self.update_corr()
            self.new_h()
            self.new_diff()
            self.update_w_o_j()

    def run(self, step, output, sample_maze, results_handler, animation):
        if animation:
            self.animate()
        i = 1
        # step = -1 imply end on reward condition so step is unbounded
        if step == -1:
            step = float('inf')
        self.set_features(self.current_state)
        # handle the first (S,A,S') observation

        # observe state S
        self.sr_state = self.new_sr_state()
        self.global_sr_state = self.new_global_sr_state(self.sr_state)
        self.state = self.v_value(self.sr_state)
        self.global_state = self.new_global_state(self.state)

        # new action
        q_values = self.new_q(self.features)
        self.action = self.new_action(q_values, sample_maze)
        self.features, bump_to_wall = self.new_features(self.current_state, self.action)

        # new reward
        self.current_reward = self.new_reward(self.features, sample_maze, bump_to_wall)

        # observe state S'
        self.sr_state_prime = self.new_sr_state()
        self.global_sr_state_prime = self.new_global_sr_state(self.sr_state_prime)
        self.state_prime = self.v_value(self.sr_state_prime)
        self.global_state_prime = self.new_global_state(self.state_prime)

        self.new_signals()
        self.new_global_signal()

        # update
        self.update(sample_maze)
        if output:
            results_handler.parse_results_between_cycle(self)

        while i < step and self.current_reward <= 0.0:
            i += 1
            self.state = self.state_prime
            self.global_state = self.global_state_prime
            self.sr_state = self.sr_state_prime
            self.global_sr_state = self.global_sr_state_prime

            # new action
            q_values = self.new_q(self.features)
            self.action = self.new_action(q_values, sample_maze)
            self.features = self.new_features(self.current_state, self.action)

            # new reward
            self.current_reward = self.new_reward(self.features, sample_maze, bump_to_wall)

            # observe state S'
            self.sr_state_prime = self.new_sr_state()
            self.global_sr_state_prime = self.new_global_sr_state(self.sr_state_prime)
            self.state_prime = self.v_value(self.sr_state_prime)
            self.global_state_prime = self.new_global_state(self.state_prime)
            self.new_signals()
            self.new_global_signal()

            # update
            self.update(sample_maze)

            if animation:
                self.animate()
                print("state = {}".format(self.state))
                print("state_prime = {}".format(self.state_prime))
                print('norm w_j = {}'.format(np.linalg.norm(self.w_j)))
                print("norm w_ij = {}".format(np.linalg.norm(self.w_ij)))
                print("norm w_ij_sr_td = {}".format(np.linalg.norm(self.w_ij_sr)))
        print(utils.softmax(self.cred))
        if output:
            results_handler.parse_results_between_cycle(self)
        return i
