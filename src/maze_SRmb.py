from maze_base import MazeBase
import numpy as np
from numpy.linalg import inv


class MazeMB(MazeBase):
    def __init__(self, maze_file):
        super().__init__(maze_file)
        self.gamma_policy = 0.0
        self.first_run_completed = False
        self.t = None
        self.policy = np.repeat(1 / self.nb_action, self.nb_cells * self.nb_action).reshape(
            (self.nb_cells, self.nb_action))

    def set_params(self, params):
        self.gamma_td = params["gamma_td"]
        self.gamma_policy = params["gamma_policy"]
        self.discount = params["discount"]
        self.epsilon = params["epsilon"]
    """
    Update SR matrix 
    """
    def new_sr_matrix(self):
        self.sr_matrix = inv(np.eye(self.nb_cells) - self.discount * self.t)

    def eval(self, state):
        if self.first_run_completed:
            self.new_t()
            self.new_sr_matrix()
        self.new_v()
        return self.new_q(state)
    """
    Update policy
    
    """
    def new_policy(self):
        outcome = np.zeros((self.nb_action,))
        outcome[self.action_prime] = 1
        self.policy[self.state_prime, :] = \
            self.gamma_policy * outcome + (1 - self.gamma_policy) \
            * self.policy[self.state_prime, :]
    """
    Update new transition matrix
    
    """
    def new_t(self):
        self.t = np.zeros((self.nb_cells, self.nb_cells), float)
        for s in range(self.nb_cells):
            sp = np.zeros((self.nb_cells,))
            sp[self.tr_matrix[s, :]] = self.policy[s, :]
            bump_to_wall_pos = [i for i, x in enumerate(self.tr_matrix[s, :]) if x == s]
            sp[bump_to_wall_pos] = 0
            sp_total = np.sum(sp)
            if sp_total != 0:
                sp /= sp_total
            self.t[s:] = sp

    def run(self, step, output, sample_maze, results_handler, animation):
        i = super(MazeMB, self).run(step, output, results_handler, animation)
        self.first_run_completed = True
        return i

    def update(self, sample_maze):
        if not sample_maze:
            self.new_w()
        self.new_policy()

    def reset_maze(self):
        super(MazeMB, self).reset_maze()
