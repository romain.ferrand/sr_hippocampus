\begin{thebibliography}{10}
\providecommand{\url}[1]{\texttt{#1}}
\providecommand{\urlprefix}{URL }
\providecommand{\doi}[1]{https://doi.org/#1}

\bibitem{aarts2000habits}
Aarts, H., Dijksterhuis, A.: Habits as knowledge structures: Automaticity in
  goal-directed behavior. Journal of personality and social psychology
  \textbf{78}(1), ~53 (2000)

\bibitem{andersen2006hippocampus}
Andersen, P., Morris, R., Amaral, D., Bliss, T., O'Keefe, J.: The hippocampus
  book. Oxford university press (2006)

\bibitem{bonet2007speed}
Bonet, B.: On the speed of convergence of value iteration on stochastic
  shortest-path problems. Mathematics of Operations Research  \textbf{32}(2),
  365--373 (2007)

\bibitem{carr2011hippocampal}
Carr, M.F., Jadhav, S.P., Frank, L.M.: Hippocampal replay in the awake state: a
  potential substrate for memory consolidation and retrieval. Nature
  neuroscience  \textbf{14}(2), ~147 (2011)

\bibitem{crosby2019animal}
Crosby, M., Beyret, B., Halina, M.: The animal-ai olympics. Nature Machine
  Intelligence  \textbf{1}(5), ~257 (2019)

\bibitem{dayan1993improving}
Dayan, P.: Improving generalization for temporal difference learning: The
  successor representation. Neural Computation  \textbf{5}(4),  613--624 (1993)

\bibitem{doll2012ubiquity}
Doll, B.B., Simon, D.A., Daw, N.D.: The ubiquity of model-based reinforcement
  learning. Current opinion in neurobiology  \textbf{22}(6),  1075--1081 (2012)

\bibitem{fyhn2007hippocampal}
Fyhn, M., Hafting, T., Treves, A., Moser, M.B., Moser, E.I.: Hippocampal
  remapping and grid realignment in entorhinal cortex. Nature
  \textbf{446}(7132), ~190 (2007)

\bibitem{gauthier2018dedicated}
Gauthier, J.L., Tank, D.W.: A dedicated population for reward coding in the
  hippocampus. Neuron  \textbf{99}(1),  179--193 (2018)

\bibitem{gershman2018successor}
Gershman, S.J.: The successor representation: its computational logic and
  neural substrates. Journal of Neuroscience  \textbf{38}(33),  7193--7200
  (2018)

\bibitem{gershman2012successor}
Gershman, S.J., Moore, C.D., Todd, M.T., Norman, K.A., Sederberg, P.B.: The
  successor representation and temporal context. Neural Computation
  \textbf{24}(6),  1553--1568 (2012)

\bibitem{glimcher2011understanding}
Glimcher, P.W.: Understanding dopamine and reinforcement learning: the dopamine
  reward prediction error hypothesis. Proceedings of the National Academy of
  Sciences  \textbf{108}(Supplement 3),  15647--15654 (2011)

\bibitem{gonner2017predictive}
G{\"o}nner, L., Vitay, J., Hamker, F.H.: Predictive place-cell sequences for
  goal-finding emerge from goal memory and the cognitive map: A computational
  model. Frontiers in computational neuroscience  \textbf{11}, ~84 (2017)

\bibitem{hafting2005microstructure}
Hafting, T., Fyhn, M., Molden, S., Moser, M.B., Moser, E.I.: Microstructure of
  a spatial map in the entorhinal cortex. Nature  \textbf{436}(7052), ~801
  (2005)

\bibitem{jakel2009does}
J{\"a}kel, F., Sch{\"o}lkopf, B., Wichmann, F.A.: Does cognitive science need
  kernels? Trends in cognitive sciences  \textbf{13}(9),  381--388 (2009)

\bibitem{khamassi2005actor}
Khamassi, M., Lach{\`e}ze, L., Girard, B., Berthoz, A., Guillot, A.:
  Actor--critic models of reinforcement learning in the basal ganglia: from
  natural to artificial rats. Adaptive Behavior  \textbf{13}(2),  131--148
  (2005)

\bibitem{miller2019habits}
Miller, K.J., Shenhav, A., Ludvig, E.A.: Habits without values. Psychological
  review  (2019)

\bibitem{mnih2016asynchronous}
Mnih, V., Badia, A.P., Mirza, M., Graves, A., Lillicrap, T., Harley, T.,
  Silver, D., Kavukcuoglu, K.: Asynchronous methods for deep reinforcement
  learning. In: International conference on machine learning. pp. 1928--1937
  (2016)

\bibitem{mnih2015human}
Mnih, V., Kavukcuoglu, K., Silver, D., Rusu, A.A., Veness, J., Bellemare, M.G.,
  Graves, A., Riedmiller, M., Fidjeland, A.K., Ostrovski, G., et~al.:
  Human-level control through deep reinforcement learning. Nature
  \textbf{518}(7540), ~529 (2015)

\bibitem{momennejad2012human}
Momennejad, I., Haynes, J.D.: Human anterior prefrontal cortex encodes the
  ‘what’and ‘when’of future intentions. Neuroimage  \textbf{61}(1),
  139--148 (2012)

\bibitem{Momennejad449470}
Momennejad, I., Howard, M.W.: Predicting the future with multi-scale successor
  representations. bioRxiv  (2018). \doi{10.1101/449470},
  \url{https://www.biorxiv.org/content/early/2018/10/22/449470}

\bibitem{o1978hippocampus}
O'keefe, J., Nadel, L.: The hippocampus as a cognitive map. Oxford: Clarendon
  Press (1978)

\bibitem{scikit-learn}
Pedregosa, F., Varoquaux, G., Gramfort, A., Michel, V., Thirion, B., Grisel,
  O., Blondel, M., Prettenhofer, P., Weiss, R., Dubourg, V., Vanderplas, J.,
  Passos, A., Cournapeau, D., Brucher, M., Perrot, M., Duchesnay, E.:
  Scikit-learn: Machine learning in {P}ython. Journal of Machine Learning
  Research  \textbf{12},  2825--2830 (2011)

\bibitem{post1930generalized}
Post, E.L.: Generalized differentiation. Transactions of the American
  Mathematical Society  \textbf{32}(4),  723--781 (1930)

\bibitem{POUCET201751}
Poucet, B., Hok, V.: Remembering goal locations. Current Opinion in Behavioral
  Sciences  \textbf{17},  51 -- 56 (2017).
  \doi{https://doi.org/10.1016/j.cobeha.2017.06.003},
  \url{http://www.sciencedirect.com/science/article/pii/S2352154616302832},
  memory in time and space

\bibitem{qasim2018neurons}
Qasim, S.E., Miller, J., Inman, C.S., Gross, R., Willie, J.T., Lega, B., Lin,
  J.J., Sharan, A., Wu, C., Sperling, M.R., et~al.: Neurons remap to represent
  memories in the human entorhinal cortex. bioRxiv p. 433862 (2018)

\bibitem{russek2017predictive}
Russek, E.M., Momennejad, I., Botvinick, M.M., Gershman, S.J., Daw, N.D.:
  Predictive representations can link model-based reinforcement learning to
  model-free mechanisms. PLoS computational biology  \textbf{13}(9),  e1005768
  (2017)

\bibitem{sarel2017vectorial}
Sarel, A., Finkelstein, A., Las, L., Ulanovsky, N.: Vectorial representation of
  spatial goals in the hippocampus of bats. Science  \textbf{355}(6321),
  176--180 (2017)

\bibitem{shankar2013optimally}
Shankar, K.H., Howard, M.W.: Optimally fuzzy temporal memory. The Journal of
  Machine Learning Research  \textbf{14}(1),  3785--3812 (2013)

\bibitem{stachenfeld2017hippocampus}
Stachenfeld, K.L., Botvinick, M.M., Gershman, S.J.: The hippocampus as a
  predictive map. Nature neuroscience  \textbf{20}(11), ~1643 (2017)

\bibitem{sutton2011reinforcement}
Sutton, R.S., Barto, A.G.: Reinforcement learning: An introduction

\end{thebibliography}
