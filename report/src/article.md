---
title: Efficient synaptic transmission in rate-coded neural networks on parallel hardware
author:
    - Helge Ülo Dinkelbach$^{1, *}$
    - Julien Vitay$^{1}$
    - Fred H. Hamker$^{1}$
corrauthor: Helge Ülo Dinkelbach
shorttitle: Rate-coded neural networks on parallel hardware
institution: $^{1}$Faculty of Computer Science, Chemnitz University of Technology, Straße der Nationen 62, D-09107 Chemnitz, Germany
email: helge-uelo.dinkelbach@cs.tu-chemnitz.de
abstract: Modern multi-core CPUs an GPUs offer high computational power usable on shared memory systems. However, the efficient usage of such hardware requires a considerable programming effort. Code generation allows adapting the implementation of a specific neural network based on high-level definition, thus gaining recently an increasing interest in the development of neuro-simulators. Synaptic transmission is one of the most time-consuming operations in large-scale rate-coded neural networks. The structure of this operation resembles a sparse matrix-vector multiplication (SpMV), which is a well-investigated kernel in the parallel computing community. Several formats and extensions were proposed over the last decades and compared on an increasing set of matrices. In many cases, there is no "one-size-fits-all" format, rather several factors should be taken into consideration. We experimentally compare three sparse matrix formats (CSR, ELLPACK, CSB) regarding their parallel performance on multi-core CPUs and GPUs using typical connectivity matrices used in biologically realistic neural networks. Our results show that CPUs and GPUs require different sparse matrix representations to achieve maximum performance and that the optimal data structure depends on the connectivity statistics, arguing for a thorough analysis before code generation.
keywords: neural networks, parallel computation, openMP, openMPI, CUDA, shared memory systems
---

# Introduction

The parallel implementation of synaptic transmission in spiking networks is broadly investigated on single- and multi-core CPUs \citep[e.g.][]{Brette2007, Plesser2007, Brette2011, Kunkel2014, Pecevski2014, Jordan2018} and GPUs \citep[e.g.][]{Nageswaran2009, Fidjeland2009, Fidjeland2010, Nowotny2011, Yavuz2015}. Even though several simulation tools for the development of rate-coded models exist, e. g. Emergent \citep{Aisa2008}, Topographica \citep{Bednar2009}, DANA \citep{Rougier2012} or ANNarchy \citep{Vitay2015}, synaptic transmission in rate-coded neural networks is less investigated aside from problem-specific implementations outside of neuro-simulators. The parallel implementation of rate-coded neural networks in neuro-simulators is seldom investigated in detail \citep{Dinkelbach2012, Vitay2015}. Recently, \cite{Hahne2017} extended the NEST simulation tool with limited rate-coded support and demonstrated the scalability for rate-coded and spiking sample networks. We do not discuss here similar work on convolutional neural networks on GPUs \citep{Mutch2010, Ciresan2010, Ciresan2012}, artificial neural networks on GPUs using radial basis functions \citep{Brandstetter2008} or multi-layer perceptrons \citep{Oh2004},as they deal with different kinds of neural networks with different computational requirements. The effectiveness of these implementations originates in particular in the structure of the networks and the simple activation functions used in the neurons.

In this article, we compare different implementations of the synaptic transmission on multi-core CPUs and GPUs. The output of a standard rate-coded neuron can be described as a first-order ordinary differential equation (ODE) integrating a weighted sum of its inputs:

\begin{equation}
    \begin{aligned}
    \tau \frac{d m_j}{dt} + m_j &= \sum^N_{i=1} w_{ij} \, r_i \\
    r_j &= f(m_j)
    \end{aligned}
\end{equation}

where $m_j$ and $r_j$ are respectively the membrane potential and the firing rate of the neuron of index $j$, $\tau$ is a time constant and $w_{ij}$ is the synaptic weight between two connected neurons $i$ and $j$. The function $f$ is an activation function, for example a sigmoid or a piecewise linear function. For rate-coded neural networks, synaptic transmission - computing the weighted sum of inputs - is the main performance limiter, as the computational effort of the evaluation of the ODE requires a comparatively small computational time and can be parallelized efficiently \citep{Dinkelbach2012}. This is in opposition to event-driven transmission in spiking networks, where only a subset of connections is activated at each time step depending on the spiking activity \citep{Brette2007, Brette2011}.

The synaptic weights between two populations of neurons can be represented by an $M$ times $N$ connectivity matrix, where the $M$ rows represent the post-synaptic neurons and the $N$ columns the pre-synaptic neurons. Fig. \ref{fig:connectivity} shows an example of a small neural network and the corresponding connectivity matrix.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.8\textwidth]{images/connectivity.png}
    \caption{Simple network conmposed of two populations of neurons (left) and the corresponding connectivity matrix (right). The rows of the connectivity matrix depict the receiving (post-synaptic) neurons and the columns the sending (pre-synaptic) neurons. Each element of the matrix represents the synaptic weight between two neurons.  Non-existing connections are represented by zeros.}
    \label{fig:connectivity}
\end{figure}

In the general case where two populations are sparsely connected, the evaluation of synaptic transmission is a sparse matrix-vector multiplication (short SpMV) operation between a vector of pre-synaptic firing rates and a sparse connectivity matrix. This operation is considered as memory-bound as there are $2 \cdot nnz \cdot M$ floating operations compared to at least $12 \cdot nnz + 8 \cdot (M+N)$ read bytes, whereas M and N are the matrix dimensions and nnz the number of non-zeros \citep{Filippone2017}. This approximation assumes double precision to represent the non-zeros. The computational performance of this operation is primarily determined by the number of non-zero elements in the connectivity matrix, as well as their distribution across the matrix. While each non-zero element is only accessed once in the operation, there is frequent access to the dense pre-synaptic vector at different positions. The number of these accesses should be reduced, for example through an efficient caching of this vector \citep{Williams2009}. Due to the generally unknown sparsity of a matrix, the efficient parallel implementation of the SpMV operation is considered as a hard problem \citep{Liu2015a}.

The SpMV is a central operation in many scientific applications and various data structures have been proposed over the years. \cite{Filippone2017} discussed 71 storage formats and extensions suitable for GPGPU computing and highlighted the importance of memory footprint and conversion costs between storage formats, while \cite{Langr2016} collected 66 storage formats, discussing implications on both CPUs and GPGPUs. The most popular format is the *compressed sparse row* (CSR) \citep{Blelloch1993, James1997}, which will be presented in section \ref{sec:csr} and is the most commonly used data structure in neural simulation environments to represent connectivity for both platforms CPUs and GPUs. This is reasonable as CSR does not rely too much on the structural properties of the matrix and is easy to implement with relatively low memory footprint (as discussed in section \ref{sec:matrix_formats}).

As we will point out in section \ref{sec:testset}, connectivity matrices occurring in neural simulations can have different matrix statistics compared to matrices investigated in the SpMV literature. Performance evaluations of SpMV structures are seldom bound to specific use cases but rather use a larger collection of specific matrices used in scientific computing. There exists relatively few rules yet defining in which cases a given format is more performant than another. \cite{Vazquez2011} and \cite{Sedaghati2015} suggests for example the density of a matrix as a guiding factor for the selection of a particular data structure. \cite{Langr2016} propose the computation time relative to CSR and the conversion costs to CSR as two important metrics for algorithm comparison. The conversion costs can be rather important for the full implementation of scientific algorithms \citep{Greathouse2014}. In addition, the memory footprint of an implementation is an important issue for GPU implementations. It is still unclear which factor are relevant for the simulation of rate-coded neural networks.

The focus of this article relies on the usage of different SpMV proposed for usage on multi-core CPUs and GPUs to implement synaptic transmission in rate-coded networks on different connectivity patterns. As in our previous work \citep{Dinkelbach2012}, we assume that the precise structure of the matrix is \textit{a priori} unknown to the algorithm. This article extends our previous work in three major directions: a) we investigate more array-based formats, b) use an extended test set of matrices and c) add an MPI implementation for multi-core systems. We will show that the efficient parallelization on GPUs and multi-core CPUs require different sparse matrix formats. Section \ref{sec:matrix_formats} introduces the sparse matrix formats used in this article. Section \ref{sec:implementation} describes the implementation of the synaptic transmission / SpMV operations using the parallel paradigms openMP, MPI and CUDA. Section \ref{sec:results} provides a performance analysis of sparse matrix formats on multi-core CPUs as well the GPU.

# Material and methods

## Matrix formats
\label{sec:matrix_formats}

Sparse matrix formats represent the non-zero elements of the matrix at the cost of introducing additional arrays \citep{Wilkinson1971}. Using the structural properties of the matrix (e.g. the number of non-zeros, the number of non-zeros per row or the distribution of non-zeros across the matrix), these formats can lead to large performance gains, for example when the non-zero elements are stored on a diagonal or clustered in dense sub-matrices \citep{Blelloch1993}. In this article, we will focus on sparse matrix formats which are not focused on specific matrix properties, as the structure of a connectivity matrix is a priori unknown in a general purpose simulation environment.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=\textwidth]{images/spm_formats.png}
    \caption{Comparison of different sparse matrix formats (dense, CSR, CSB, ELLPACK and ELLPACK-R). In case of a CSR, each non-zero is represented by a value and a column index. The `row\_ptr` vector indicates the start and end of a row. The CSB format splits the matrix into blocks of size $\beta$. Non-zeros within a block are described by their row, column index and value. Those indices are relative to the border, within the range 0 to $\beta - 1$, consequently the number of bytes to represent the indices can be reduced. The ELLPACK format represents the non-zero in two matrices of dimension $N$ times the maximum number of non-zeros per row. If an index is not filled, this is indicated by a pre-defined value, often 0.}
    \label{fig:data_structure_comparison}
\end{figure}

Fig. \ref{fig:data_structure_comparison} depicts the formats investigated in this article: dense, CSR, CSB, ELLPACK and ELLPACK-R. The following sections will focus on the conceptual aspects of these formats. Their impact on the parallel performance of the synaptic transmission is discussed in section \ref{sec:implementation}.

### Dense matrix

A dense matrix representation requires only one array with $M$ times $N$ elements. In the parallel computing literature, non-zeros are often set to 0 as it is a neutral value in the SpMV operation. The memory consumption (Eqn. \ref{eq:mem_dense_matrix}) is easy to determine (Eq. \ref{eq:mem_dense_matrix}). In this article, we assume double precision in this equation, otherwise, the memory requirement for weight values would be halved.

\begin{equation}
    \text{mem}_{dense} = M \cdot N \cdot 8
    \label{eq:mem_dense_matrix}
\end{equation}

The choice of 0 as a representation of non-existing synapses is problematic for neural networks: as a result of learning, a weight value of 0 is possible, without implying that the synapse does not exist. Synaptic plasticity might later change the weight value for such a synapse, while this should not be possible for non-existing synapses. Using different default values (e.g. -1) does not solve the problem, while making the matrix-vector multiplication harder to implement (addition of code branches). A more efficient approach is to add an additional boolean matrix to encode existing connection entries. Multiplying the weight change with the value in the boolean matrix would avoid injecting code branches, at the cost of higher memory consumption and an additional multiplication. However, such an SpMV implementation can be vectorized with only a few efforts.

### Compressed Sparse Row (CSR)
\label{sec:csr}

Compressed sparse row (CSR), also called compressed row storage (CRS) or Yale format, is probably the most used sparse matrix format \citep{Blelloch1993}. The CSR representation of a connectivity matrix consists of three parts: two linear arrays, one for the pre-synaptic ranks (Column) and one for the synaptic weight (Values). The third array (Row_ptr) describe the number of non-zero entries for a single post-synaptic neuron: the synapses related to the $i$-th post-synaptic neuron range from the $i$ to $i+1$ indices. The memory consumption (Eq. \ref{eq:mem_csr}) is consequently dominated by the number of non-zeros. If we assume *integer* as index type with 4 bytes and *double* as floating precision with 8 bytes, the memory consumption is:

\begin{equation}
    \label{eq:mem_csr}
    \text{mem}_\text{CSR} = \underbrace{(M+1) \cdot 4}_{\text{row\_ptr}} + \underbrace{nnz \cdot 4}_{\text{col\_idx}} + \underbrace{nnz \cdot 8}_{\text{values}}
\end{equation}

### ELLPACK and ELLPACK-R

The ELLPACK format \citep{Kincaid1989} was introduced to allow vectorization by using dense matrices for the indices and values. One first has to determine the maximum length of the rows ($max_{nnz}$) and construct two dense matrices with the dimensions $\#rows$ times $max_{nnz}$, one for the column indices and one for the values. If a row is shorter than $nnz_{max}$, the remaining spaces are filled with a fixed value (typically 0 or -1). As for dense matrices, many implementations use 0 as it is a neutral element for the SpMV operation. The parameter $max_{nnz}$ can also be fitted to the length of SIMD operations or cache-line lengths. If the $max_{nnz}$ is limited, other values could be stored then in a coordinate format (COO) comprising three arrays: one for the row indices, one for column indices and one for values. The resulting combination of an ELLPACK and the coordinate (COO) format is known as hybrid (HYB) format \citep{Sedaghati2015}.

\begin{equation}
    \label{eq:mem_ell}
    \text{mem}_\text{ELLPACK} = \underbrace{ M \cdot max_{nnz} \cdot 4 }_{\text{Row\_ptr}} + \underbrace{M \cdot max_{nnz} \cdot 8}_{\text{Columns}}
\end{equation}

\cite{Vazquez2011} proposed a modification of the ELLPACK format, called ELLPACK-R, which introduces a row length (*rl*) array encoding for each row the number of non-zeros. Unnecessary operations like the multiplication of $w = 0$ and $col\_idx = 0$ can be avoided with this addition. However, the memory consumption is slightly increased (Eq. \ref{eq:mem_ellr}), but the overhead seems to be negligible considering the size of the dense matrices.

\begin{equation}
    \label{eq:mem_ellr}
    \text{mem}_\text{ELLPACK-R} = \underbrace{ ( M + 1 ) \cdot 4 }_{\text{row length}} + \underbrace{ M \cdot max_{nnz} \cdot 4 }_{\text{column index}} + \underbrace{M \cdot max_{nnz} \cdot 8}_{\text{values}}
\end{equation}

The most important point is that the obtained arrays are stored in column-major order. This leads to long rows of continuous data, which is nice for SIMD like operations. Obviously, the ELLPACK/ELLPACK-R formats have a major problem: empty rows and strong variations in the number of non-zeros per row potentially lead to larger memory footprints compared to the other formats.

### Compressed Sparse Blocks (CSB)

Compressed Sparse Blocks (CSB) was proposed by \cite{Buluc2009} to be effective for both matrix-vector and transposed matrix-vector multiplications while maintaining a memory consumption comparable to CSR. It can be even lower, as discussed in section \ref{sec:testset}, if the number of bytes for the index type representation can be reduced. Like in CSR structures, there is a dense top-level array `blk\_ptr`, which identify the start and end of the single blocks. In total, the matrix is split into $\frac{M \cdot N}{\beta^2}$ blocks. In the original publication, they concentrate on equal sized blocks but the format is also applicable to non-squared matrices by choosing $\beta$ differently for each dimension. Each block is then a sparse representation of the non-zero elements, where the indices are relative to the block borders. This is important, as now the index data type of *row_idx* and *col_idx* can be reduced from 4 to 1 byte if $\beta < 256$, reducing consequently the memory footprint.

\begin{equation}
    \label{eq:mem_csb}
    \text{mem}_{CSB} = \underbrace{ \frac{M \cdot N}{ \beta  \cdot \beta} \cdot 4}_{\text{blk\_ptr}} + \underbrace{nnz \cdot 1}_{\text{row\_idx}} + \underbrace{nnz \cdot 1}_{\text{col\_idx}} + \underbrace{nnz \cdot 8}_{\text{values}}
\end{equation}

## Implementation
\label{sec:implementation}

The SpMV operation is memory-bound \citep{Gropp1999, Goumas2008, Williams2009, Yzelman2014} and impaired by irregular data accesses on CPUs \citep[e. g.][]{Goumas2008, Yzelman2014} and GPUs \citep{Bell2009, Greathouse2014}. \cite{Vuduc2003} and \cite{Vuduc2005} claim that pure CSR implementations reach a limit at 10\% or less of CPU peak performance. (**what does this mean?**) \cite{Yzelman2014} claim that issues like irregular access and being memory-bound limit the single thread performance and can have even more bad effects if not handled for parallel implementations. Aside from proposing new formats some authors propose suggestions for load balancing, for instance matrix reordering (e. g. \cite{Pinar1999}) or extension of existing formats, e. g. ICSR which adds zero elements to rows \citep{Yang2012}. Among all the work done on the SpMV one can notice two general problems faced in single-thread as well as parallel implementations: a reusage of the dense vector (in our case the *pr* and *psp*) and avoiding of misaligned memory accesses.

The following sections introduce the implementation of the above formats for multi-core CPUs and GPUs using different paradigms: single thread, OpenMP, MPI and CUDA. Within the code snippets, we used two abbreviations to describe the index type (IT, typically an integer) and the value type (VT, typically a floating representation). We use the array variable names described in the previous section and depicted in Fig. \ref{fig:data_structure_comparison}.

**TODO: statement on where to find the code, as not everything is described**

### Single thread implementation

In the single thread version, synaptic transmission is implemented as a double for-loop: the outer loop index $i$ iterates over all post-synaptic neurons, the inner loop over all synapses $j$ which are reaching the neuron $i$. Fig. \ref{fig:rate_coded_st_code} shows the single thread implementation for the CSR (A), ELLPACK (B) and CSB (C) formats. All three implementations show the consecutive access to the weight array `w` and column array `col_idx` only once per kernel execution. Access to the dense array of pre-synaptic firing rate (`pr`) is however highly irregular and each element is accessed multiple times. The boundary of the inner loop in the case of ELLPACK is the maximum number of non-zeros per row `nzr`, while CSR use the boundary stored in `row_ptr` and CSB encodes the boundary of non-zeros within a block in `blk_ptr`.

The ELLPACK format requires the explicit encoding of non-existing entries, often encoded with `w`=0 and `col_idx`=0. The consecutive access to the elements in `w` and `col_idx` allows vectorization. As both values are 0, the summation of these non-existing elements does not influence the results. However, if the average number of non-zeros and `nzr` diverge significantly, these will lead to a higher number of unnecessary operations. As discussed in the previous section, the ELLPACK-R format solves this issue with an additional row length array `rl`. An ELLPACK-R of the inner loop (Fig. \ref{fig:rate_coded_st_code}B, line 4) would replace the `nzr` by `rl[i]` as boundary. Alternatively, one could use a hybrid format, creating an ELLPACK matrix with `nzr` set to the average number of non-zeros and all remaining elements stored with the coordinate format. In any case, the column-oriented storage and access (via `idx` on line 5) to the weights and column indices are important for this particular data format.

The CSB implementation divides the connectivity matrix in `br` times `bc` blocks depending on the block size $\beta$. $\beta$ could be chosen differently for both dimensions if matrices are not squared. As the index type IT is `uint8_t` for this kernel, which allows the use of values from 0 to 255 as possible ranks within one block, there is a larger possible range for the parameter $\beta$, whose influence on the performance is out of the scope of this article. We choose a block size $\beta=32$, fulfilling the requirements suggested by \cite{Buluc2009}, and the number of blocks in a row (`br`) and number of blocks in a column (`bc`) are precomputed.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=.8\textwidth]{images/rate_code_psp_st_code.png}
    \caption{Code snippets for the single thread implementation of the SpMV operation for different sparse matrix formats: A) compressed sparse row (CSR), B) ELLPACK and C) compressed sparse block (CSB). The result of the kernel is the dense vector \texttt{psp}, the input is the dense vector \texttt{pr} encoding the pre-synaptic firing rate. The sparse matrix is differently represented among the formats, but contains at minimum two arrays \texttt{w} and \texttt{col\_idx}.}
    \label{fig:rate_coded_st_code}
\end{figure}

Fig. \ref{fig:rate_coded_kernel} visualizes the different access patterns depending on the data structure: A) CSR, B) ELLPACK and C) CSB. The access to the pre-synaptic firing rates is scattered and is hardly cacheable for all formats, while the access to column indices and synaptic weights has a higher probability to be cached. The block-wise representation of the CSB matrix representation has an additional benefit related to the access to the dense vector. As each block is limited within the range $\beta$, both dense vectors `psp` as well as `rate` can be loaded at the begin of the computation of the current block if $\beta$ is chosen as a multiple of the cache line width, ensuring a higher cache friendliness than the CSR format.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=.9\textwidth]{images/rate_coded_psp_kernel.png}
    \caption{Representation of access patterns for the formats A) CSR, B) ELLPACK and C) CSB. The result of the kernel is the dense vector \texttt{psp}, the input is a dense vector \texttt{rate} encoding the pre-synaptic firing rates. The colors indicate different neurons, i. e. orange encodes the first neuron, blue the second and green the fourth. The arrows indicate the access pattern for single accesses. The access to \texttt{rate} is scattered and will lead to higher cache misses. This problem is reduced for the CSB format. Because of the use of dense matrices, the ELLPACK format allows the vectorization. CSB eases the vectorization and ensures a high cache friendliness if the block size $\beta$ (here noted as B) is chosen as multiple of the cache length.}
    \label{fig:rate_coded_kernel}
\end{figure}

\clearpage

### OpenMP implementation
\label{openmp_impl}

OpenMP is a language extension for C, C++ and Fortran which provides a set of compiler directives for the easier implementation of parallel programs for shared-memory systems. Many available compilers have OpenMP compatibility and provide it through compiler flags. Already existing serial code is often reusable, so in general, no explicit parallel programming or attention should be required, as preexisting loops are only annotated with special directives. In addition to the number of working threads, the number of elements computed per thread is controllable: for example, each thread can be responsible for fifty elements in a for-loop over thousand elements. Data created or used in a parallel region is shared by all threads within the working group even if they are scattered across CPUs. Consequently, the programmer needs to ensure that write accesses to variables are thread safe, what will impair the overall performance.

For synaptic transmission, OpenMP parallelization can be easily implemented by adding a parallel pragma `#pragma omp parallel for` on the outer loop of the single thread implementations (**TODO: check**). Each thread computes a set of $\frac{N}{T}$ rows, independently from the other threads in a working group so there is no additional synchronization necessary. However, access to the result vector `psp` can lead to false sharing, i.e. two threads access neighboring data and induce synchronization without truly accessing the same values, which as a result will lower the achieved performance. 

Nowadays available shared memory systems often use a Non-Uniform Memory Access (NUMA) memory architecture. In such a system the available memory is divided into chunks assigned directly to the CPUs\footnote{A multi-core CPU in a NUMA system is often referred as socket}. Each CPU has an own memory controller to manage these chunks. This improves the memory bandwidth for each CPU but introduces additional overhead if one CPU wants to access the memory assigned to another CPU. This overhead can impair openMP implementations if shared array needs to be synchronized over multiple sockets. This can be prevented by using temporary thread local arrays, which are fused afterwards.

### MPI implementation

OpenMPI \citep{Graham2006} is a widely used implementation of the Message Passing Interface (MPI) API and available as a library for many operating systems. MPI defines the communication interface between processes providing synchronization and collective operation commands. Contrary to openMP, an (open)MPI implementation requires explicit programming of communication between the processes. MPI targets primarily computation on distributed memory systems, but several researchers worked on the efficient implementation of the MPI standard on shared-memory systems \citep[e.g.][]{Graham2008}. \cite{Rabenseifner2009} discussed hybrid implementations of MPI and openMP as an approach to reduce communication needs or load imbalances. Memory consumption can be reduced as less duplicated data, e.g. buffers for communication, need to be stored. Other hybrid approaches use multiple GPUs next to openMP \citep{Yang2011}. As described by \cite{Kunkel2014}, the NEST kernel uses such a hybrid implementation to reduce process overheads.

Depending on the number of created MPI processes $N_P$, the matrix is divided into equally sized parts across the row dimension in our implementation. \cite{James1997} call this strategy "owner-computes". We compare two implementation variants in this article: pure MPI and hybrid MPI. In case of the pure MPI implementation, we have $N_P = [1..16]$ processes, each of them executing the single thread code on his partition. For the hybrid implementation, we use $N_P=2$ as we had 2 NUMA sockets. Each of these processes then spawns up to 8 OpenMP threads to compute their partition, using the previously described OpenMP code (section \ref{openmp_impl}). In both variants, the MPI processes do not need to communicate the results (the `psp` vector), as these results would be further processed only locally. The only required communication is the exchange of pre-synaptic firing rates which need to be done at each time step.

**TODO: code example?**

### CUDA implementation

The CUDA framework proposed by NVIDIA \citep{Nickolls2008} allows to write C-like code and execute it on massively parallel graphics processing units (GPUs) and make them usable for general purpose computation. Programming in CUDA requires the explicit implementation of data transfers between host (CPU) and device (GPU). CUDA devices comprise several streaming multiprocessors (SM). Each of these processors consists of a number of CUDA cores, a data controller and a scheduler. The number of CUDA cores and scheduler is dependent on the hardware architecture. CUDA cores are handled in groups, the so-called warps, which contain on most architectures 32 CUDA cores. All threads within a warp access the same resources and execute the same instructions in parallel. Memory accesses are executed as 32, 64 or 128 byte segments (comparable to caching on CPUs). If all threads can use data at once for the execution of their current instruction, this access is called *coalesced*. Threads which require diverging data are stalled, the same applies to instruction divergence for instance as a result of if-else statements.

From the programming point of view, a CUDA device function describes the behavior of a single CUDA thread. Next to this description, the programmer provides a kernel configuration. The CUDA threads are organized hierarchically: on top is the grid which consists of blocks. Each of these computation blocks comprises of a set of CUDA threads. The index of a CUDA thread is dependent on the thread configuration provided as CUDA function argument and as a consequence, each CUDA thread has built-in indices on a grid, block and thread level. The computation time taken by a CUDA implementation depends on several factors such as memory bandwidth, register usage or coalescence in addition to the number of threads used for the kernel \citep{Bell2009, Dinkelbach2012}. Two kinds of memory are available: a global memory used by all active blocks and a shared memory available to each block. The latter one can be used for the fast exchange of data between the threads of a block but it is limited to 48 kB on most cards.

We now discuss the CUDA implementation of CSR and ELLPACK-R on GPUs (**TODO: explain why not CSB and ELLPACK**). For CSR we compare three parallelization variants: a scalar, a vector and an extended vector variant. Both scalar and vector variants were proposed by \cite{Bell2009}, while the extended vector variant was published in \cite{Dinkelbach2012}. The ELLPACK-R implementation was originally published in \cite{Vazquez2011}. The major goal in a GPU implementation of the memory-bound SpMV operation is two-fold: reduce thread divergence and ensure memory coalescence.

*Scalar:* In the CSR *scalar* implementation of CSR, each CUDA thread computes one row of the matrix as depicted in Fig. \ref{fig:rate_coded_cuda_code}A. For a larger number of non-zeros per row, this leads to uncoalesced memory accesses. For more than 16 non-zeros per row, the threads in a warp will be executed sequentially.


**TODO: is it important? If not, remove** However as shown by \cite{Bell2009}, this implementation performs well for a small number of non-zeros, e. g. the Epidemiology matrix (4 non-zeros per row). Considering this matrix with 4 non-zeros per row, this means with each memory load of e. g. 128 bytes up to 4 (double precision) or 8 (single precision) CUDA threads can use the values from *col\_idx* for computation. The other threads within the thread group are stalled.

*Vector:* To reduce the number of uncoalesced memory accesses, the vector implementation assigns to each warp one row of the matrix. The kernel consists of two stages: a thread local partial sum followed by a parallel reduction. In the first stage, each thread computes a partial sum dependent on his thread ID, i.e. a thread $t$ sums up the $0+t$, $2+t$, ... $2n+t$ elements, where $n = \frac{row\ length}{block\ size}$. These partial sums are stored in a shared memory unique to each block. In the second stage, these partial sums are accumulated using a reduction operation depicted in Fig. \ref{fig:rate_coded_cuda_code}C on the right side. More details on this method can be found for example in \cite{Nickolls2008} or \cite{Harris2007}.

*Extended vector:* The extended vector implementation is based on the vector implementation of \cite{Bell2009} but uses multiple warps together for one row, following the parallel reduction examples of \cite{Nickolls2008} and \cite{Harris2007}. This is suitable as our test cases often have hundreds or thousands of elements per row. As described in \cite{Dinkelbach2012}, there is an additional reduction stage before the warp-wide reduction (**TODO: explain**). The local summation is similar to the vector implementation. Fig. \ref{fig:rate_coded_cuda_code}C) depicts the general scheme of the SpMV using a CSR and reduction. For this example, we limited the number of threads per block to 2.

ELLPACK-R is implemented as in \cite{Vazquez2011} and the code is depicted in Fig. \ref{fig:rate_coded_cuda_code}B. As in the scalar variant, each CUDA thread computes one row, but the accesses are now more coalesced, as the weight and column index matrices were stored in column-major order. The original ELLPACK format had no branches induced by if-statements as the matrix dimension was the number of rows times the maximal number of non-zero per rows. The ELLPACK-R format uses a row length array `rl` and therefore limits the number of iterations done by a thread. this does not lead directly to thread divergence as the threads within a warp can work homogeneously if there is enough data to process and threads with no elements to compute will do nothing.

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.92\textwidth]{images/rate_coded_psp_processing.png}
    \caption{CUDA implementation of the SpMV operation for different sparse matrix formats: A) scalar CSR, B) ELLPACK-R, C) extended vector CSR. The result of the kernels is a dense vector \texttt{psp}, the input is a dense vector \texttt{rate}. The weight matrix representation for CSR and ELLPACK-R are as depicted in Fig. \ref{fig:data_structure_comparison}. A) Each thread computes the partial sum for one row in the matrix. The elements of a row are indicated by the \texttt{row\_ptr}. As each thread accesses a distinct row, all memory requests are uncoalesced. B) In the ELLPACK-R format, the threads iterate column-wise across the \texttt{weight} and \texttt{col\_idx} matrix. The \texttt{rl} array encodes how many elements in a row need to be summed up by a single thread. As the dense matrices are stored in column-major, this iteration pattern leads to coalesced accesses. As in the scalar variant, each thread computes the summation across one row. C) In the extended vector variant, a thread group computes the sum over one row. All threads within a thread group process the same row and therefore coalesced access to \texttt{col\_idx} and weights is ensured. Each thread computes a partial sum depending on its thread ID (a thread t sums up the $0+t$, $2+t$, ... $2n+t$ elements where $n = \frac{row\ length}{block\ size}$). The final sum is then computed with a parallel reduction approach on these partial sums as depicted on the right side.}
    \label{fig:rate_coded_cuda_code}
\end{figure}

\clearpage

# Results
\label{sec:results}

All tests were run on a dual socket system using Intel Xeon E5-2650 (2.60 GHz) and NVIDIA Kepler K20m. The operating system is Linux Mint 18.3 using g++ 5.4 and CUDA SDK 9.0. Additionally, we performed measurements on a system with a NVIDIA GTX1080Ti using Linux Mint 18.3, g++5.4 and CUDA SKD 9.1. All tests were repeated 5 times, where each run consists of 1000 executions of the weighted sum, and the average over the different runs is reported. More details on the performance measurement and metrics are provided in section \ref{sec:perf_measure}. In this article, we compare the achieved performance of different sparse matrix formats on a varying set of matrices, which are described in section \ref{sec:testset}. We report results for the openMP (section \ref{sec:omp_results}), openMPI (section \ref{sec:mpi_results}) and CUDA (section \ref{sec:cuda_results}) implementations and compare them to the single thread implementation (section \ref{sec:st_results}).

## Performance Measurement
\label{sec:perf_measure}

We investigate the parallel performance of different implementations and their suitability for multi-core CPUs and GPUs. We report the computation time in milliseconds and the speedup ratios relative to the single thread implementation. As demonstrated in \cite{Dinkelbach2012}, the achieved performance of a GPU implementation depends strongly on the thread per block configuration. A similar effect can be observed for multi-core implementations.

A major issue for parallel implementations is the scalability of a given implementation. In general, there exist two setups: strong and weak scaling. A detailed discussion of scalability scenarios in the context of neural simulations can be found in \citep{VanAlbada2014}. In this article, we focus on a strong scaling approach:
the computation time as a function of the number used processes/threads for a fixed problem size. We repeatedly measure the computation time for a fixed matrix configuration and varying thread configurations: 1 to 16 threads for multi-core implementations and 32 to 512 threads per block for GPUs. From the resulting vector of average performance per thread configuration, we select the lowest achieved computation time and call this *best achieved computation time*. This is then repeated for all matrix configurations.

**TODO: more details: walltime, how is it measured, etc**

**Precise that data transfer for MPI and CUDA are not measured.**
In this article we will only focus on the computation time of the kernels themselves, which means we assume, all required data is already on the GPUs. We didn't measured and report the overhead produced by the host to device memory transfers.

## Connectivity matrices
\label{sec:testset}

We use a set of different matrices for performance evaluation (Tab. \ref{tab:testset}) which differ in their number of non-zero values and their distribution. Each sparse matrix connects two populations of 40000 neurons each: as shown in \cite{Dinkelbach2012}, the number of neurons increases linearly the computation time if the number of non-zeros per neurons is fixed, so this effect is not further investigated here. Fixed probability (FP) matrices are defined by a uniform probability $p$ of creating a synapse, corresponding to the density of the sparse matrix. Fixed number pre (FNP) matrices are designed so that each row (post-synaptic neuron) has the same number of non-zeros, which are then uniformly distributed. Receptive field (RF) matrices connects a post-synaptic neuron to the pre-synaptic neuron with the same index as well as a fixed number of its neighbors (pre-synaptic neurons are arranged on a circular topology **TODO: or not? sigma is non-zero**), forming a wide diagonal matrix (**TODO: find the real name**). The dense matrices stored as sparse matrices (density of 1) serve as a demonstration for an upper performance bound but connect smaller populations \citep{Williams2009, Baskaran2008}.

**TODO: small figure to make it clearer?**

Tab. \ref{tab:testset} depicts the non-zero statistics as in \cite{Vazquez2011} and \cite{Vazquez2012}: average number of non-zeros per row ($\mu$) and deviation of the non-zeros per row ($\sigma$) as well as the relative standard deviation ($\frac{\sigma}{\mu}$). FNP matrices have no variation by design. The RF matrices have relative standard deviations between 10\% and 20 \% (**TODO: should not RF have no deviation?**). The maximal relative standard deviation of 18.47 percent is quite small in comparison to the matrices investigated in \cite{Vazquez2011} and \cite{Vazquez2012}.

\begin{table}[htbp]
    \small
    \centering
    \begin{tabular}{ | c | c | c | c | c | c | c | c | c | c |}
        \hline
    & Matrix ID & Row x Col  & non-zeros &  density [\%] & \multicolumn{5}{c |}{non-zero statistics}  \\ \cline{6-10}
    &                        &                    &  &  & $\mu $ & $\sigma$ & $\frac{\sigma}{\mu} [\%]$ & min & max \\ \hline
    FP      & 1 & 40k x 40k & 16004k & 1.00 & 400.10 & 19.91 & 4.97 &321 & 479 \\
            & 2 & 40k x 40k & 32002k & 2.00 & 800.06 & 27.97 & 3.49 & 689 & 909 \\
            & 4 & 40k x 40k & 64008k & 4.00 & 1600.19 & 39.11 & 2.45 & 1449 & 1747 \\
            & 8 & 40k x 40k & 128018k & 8.00 & 3200.44 & 54.54 & 1.70 & 2979 & 3440 \\
    \cline{1-10}
    FNP     & 16 & 40k x 40k & 640k & 0.04 & 16.00 & 0.00 & 0.00 & 16 & 16  \\
            & 32 & 40k x 40k & 1280k & 0.08 & 32.00 & 0.00 & 0.00 & 32 & 32 \\
            & 64 & 40k x 40k & 2560k & 0.16 & 64.00 & 0.00 & 0.00 & 64 & 64  \\
            & 128 & 40k x 40k & 5120k & 0.32 & 128.00 & 0.00 & 0.00 & 128 & 128  \\
            & 256 & 40k x 40k & 10240k & 0.64 & 256.00 & 0.00 & 0.00 & 256 & 256  \\
            & 512 & 40k x 40k & 20480k & 1.28 & 512.00 & 0.00 & 0.00 & 512 & 512  \\
            & 1024 & 40k x 40k & 40960k & 2.56 & 1024.00 & 0.00 & 0.00 & 1024 & 1024  \\
            & 2048 & 40k x 40k & 81920k & 5.12 & 2048.00 & 0.00 & 0.00 & 2048 & 2048  \\
            & 4096 & 40k x 40k & 163840k & 10.24 & 4096.00 & 0.00 & 0.00 & 4096 & 4096  \\
    \cline{1-10}
    RF      & 5 & 40k x 40k & 982k & 0.06 & 24.55 & 2.14 & 8.72 & 4 & 25 \\
            & 10 & 40k x 40k & 3881k & 0.24 & 97.02 & 10.13 & 10.44 & 25 & 100 \\
            & 15 & 40k x 40k & 8620k & 0.54 & 215.50 & 26.45 & 12.27 & 49 & 225 \\
            & 20 & 40k x 40k & 15132k & 0.95 & 378.30 & 51.93 & 13.73 & 100 & 400 \\
            & 25 & 40k x 40k & 23339k & 1.46 & 583.46 & 88.22 & 15.12 & 144 & 625 \\
            & 30 & 40k x 40k & 33178k & 2.07 & 829.44 & 135.28 & 16.31 & 225 & 900 \\
            & 35 & 40k x 40k & 44569k & 2.79 & 1114.22 & 194.48 & 17.45 & 289 & 1225 \\
            & 40 & 40k x 40k & 57456k & 3.59 & 1436.41 & 265.28 & 18.47 & 400 & 1600 \\
    \cline{1-10}
    Dense   & 1000 & 1k x 1k &  1000k & 100.00 & 1000.00 & 0.00 & 0.00 & 1000 & 1000 \\
            & 2000 & 2k x 2k &  4000k & 100.00 & 2000.00 & 0.00 & 0.00 & 2000 & 2000 \\
            & 4000 & 4k x 4k & 16000k & 100.00 & 4000.00 & 0.00 & 0.00 & 4000 & 4000 \\
            & 8000 & 8k x 8k & 64000k & 100.00 & 8000.00 & 0.00 & 0.00 & 8000 & 8000 \\
    \hline
    \end{tabular}
    \caption{Non-zero characteristics for the considered test set of sparse matrices. The table depicts the size of the matrix, the absolute number of non-zeros and the density of the matrix, as well as the non-zero statistics: average number of non-zeros per row ($\mu$), standard deviation of the non-zeros per row ($\sigma$) and the relative standard deviation in percent ($\frac{\sigma}{\mu} [\%]$). To highlight the spreading of the number of non-zeros per row, we also provide the minimum ($min$) and maximum ($max$) number of non-zeros in a row of the matrix.}
    \label{tab:testset}
\end{table}

Tab. \ref{tab:testset_memsize} shows the memory consumption of the matrices in Tab. \ref{tab:testset} for the considered formats in this article (CSR, ELLPACK, ELLPACK-R, CSB). The lowest requirement of memory is indicated in bold. In most cases, the memory consumption of the CSB format is lower than the other formats, a direct consequence of the 1 byte index representation. Noticeable exceptions are the matrices with a density below 0.1 percent (FNP 16, FNP 32 and RF 5). The memory consumption of CSR, ELLPACK and ELLPACK-R is comparable for all matrices of the FNP and Dense categories as the maximal row length is equal to the average row length. If this condition is not satisfied, the ELLPACK and ELLPACK-R data structure is impaired by an increasing memory overhead dependent on the difference between average and maximum row length.

\begin{table}[htbp]
    \small
    \centering
    \begin{tabular}{ | c | c | c | c | c | c | c |}
        \hline
                    & Matrix ID & density [\%] & $\text{mem}_{CSR}$ & $\text{mem}_\text{ELLPACK}$ & $\text{mem}_\text{ELLPACK-R}$ & $\text{mem}_\text{CSB}$ \\ \hline
        FP          & 1 & 1.00 & 183.30 & 219.27 & 219.42 & \textbf{158.58} \\
                    & 2 & 2.00 & 366.39 & 416.11 & 416.11 & \textbf{311.16} \\
                    & 4 & 4.00 & 732.66 & 799.71 & 799.87 & \textbf{616.38} \\
                    & 8 & 8.00 & 1465.20 & 1574.71 & 1574.86 & \textbf{1226.83} \\
        \cline{1-7}
        FNP         & 16 & 0.04 & 7.48 & \textbf{7.32} & 7.48 & 12.06 \\
                    & 32 & 0.08 & 14.80 & \textbf{14.65} & 14.80 & 18.17 \\
                    & 64 & 0.16 & 29.45 & \textbf{29.30} & 29.45 & 30.37 \\
                    & 128 & 0.32 & 58.75 & 58.59 & 58.75 & \textbf{54.79} \\
                    & 256 & 0.64 & 117.34 & 117.19 & 117.34  & \textbf{103.62} \\
                    & 512 & 1.28 & 234.53 & 234.38 & 234.38 & \textbf{201.27} \\
                    & 1024 & 2.56 & 468.90 & 468.75 & 468.90 & \textbf{396.59} \\
                    & 2048 & 5.12 & 937.65 & 937.50 & 937.65 & \textbf{787.21} \\
                    & 4096 & 10.24 & 1875.15 & 1875.00 & 1875.15 & \textbf{1568.46} \\
        \cline{1-7}
        RF          & 5 & 0.06 & \textbf{11.39} & 11.44 & 11.60 & 15.33 \\
                    & 10 & 0.24 & 44.57 & 45.78 & 45.93 & \textbf{42.97} \\
                    & 15 & 0.54 & 98.80 & 103.00 & 103.15 & \textbf{88.17} \\
                    & 20 & 0.95 & 173.33 & 183.11 & 183.26 & \textbf{150.27} \\
                    & 25 & 1.46 & 267.24 & 286.10 & 286.25 & \textbf{228.53} \\
                    & 30 & 2.07 & 379.84 & 411.99 & 412.14 & \textbf{322.37} \\
                    & 35 & 2.79 & 510.20 & 560.76 & 560.91 & \textbf{431.00} \\
                    & 40 & 3.59 & 657.69 & 732.42 & 732.57 & \textbf{553.91} \\
        \cline{1-7}
        Dense       & 1000 & 100.0 & 11.45 & 11.44 & 11.45 & \textbf{9.54} \\
                    & 2000 & 100.0 & 45.78 & 45.78 & 45.78 & \textbf{38.16} \\
                    & 4000 & 100.0 & 183.12 & 183.11 & 183.12 & \textbf{152.65} \\
                    & 8000 & 100.0 & 732.45 & 732.42 & 732.45 & \textbf{610.59} \\
        \hline
    \end{tabular}
    \caption{Memory consumption of the test matrices for the different formats. The table depicts the consumed memory in megabytes (MB). The CSB format requires the lowest memory amount, except for matrices with a density below 0.1 percent (FNP 16, FNP 32 and RF 5). CSR and ELLPACK require almost the same memory if the average and maximum row length are equal, otherwise the ELLPACK is impaired by an overhead which depends on the difference between average and maximum row length. The difference between ELLPACK-R and ELLPACK is small as the number of rows is far lower than the number of nonzeros in the matrix.}
    \label{tab:testset_memsize}
\end{table}

## Single Thread
\label{sec:st_results}

Fig. \ref{fig:rate_coded_st}A depicts the single-threaded computation time for the different test matrices, comparing the CSR (orange), ELLPACK (blue), ELLPACK-R (blue, triangles) and CSB (red) implementations. Fig. \ref{fig:rate_coded_st}B shows the ratio between each implementation and CSR. The performance on the dense matrices shows a clear dependency between the number of non-zeros (gray bars) and the achieved computation time for the compared formats. It is well-known that the SpMV operation depends on the number of non-zeros, as already demonstrated by \cite{Dinkelbach2012} on similar benchmarks. The overhead introduced by the different formats can also be observed, as performance limiters such as irregular accesses are ruled out for dense matrices.

The ELLPACK-R format is considerably slower than CSR, up to three times for the FNP matrices and five times for the RF matrices. The CSB format outperforms CSR on all FP matrices and FNP matrices with many non-zeros (FNP 256 and above), but is slower on RF matrices by a factor two. CSB is impaired by its high memory consumption for small numbers of connections, especially when they are scattered as in the FNP matrices. This is in particular a consequence of the fixed block size parameter $\beta$ = 32 chosen for all runtime configurations. For small numbers of non-zeros per row, it is likely that each non-zero is placed in a different block. In the RF pattern, the non-zeros are grouped together, leading to more regular accesses and a better performance.

\begin{figure}[htbp]
    \includegraphics[width=\textwidth]{images/rate_code_psp_st.png}
    \caption{Single thread performance achieved on the matrix test set. We compare the synaptic transmission using CSR (orange), ELLPACK (blue), ELLPACK-R (blue dashed) and CSB (red). The x-axis represents the test matrices and the y-axis A) the average computation time in milliseconds and B) the performance ratio towards CSR. The CSR format obtains the best results across all matrices. The ELLPACK format performs comparably to CSR while ELLPACK-R performs worse due to a bad memory access pattern. For CSB, the picture is not that clear: it performs generally worse than CSR and ELLPACK on most of the matrices except FP matrices, FNP 256 and FNP 512. Overall, CSR is an efficient data structure for single-thread computation on CPUs. B) The performance ratio between the different formats and CSR shows that CSR and ELLPACK behave quite similarly on the test matrices.}
    \label{fig:rate_coded_st}
\end{figure}

Although the memory consumption of ELLPACK-R and CSR are relatively close, there are large performance differences across all tested use cases. This is a consequence of the construction of the ELLPACK format (**TODO: are you talking about ELLPACK or ELLPACK-R?**): non-zeros of one row are stored continuously in CSR and column-wise in ELLPACK. For a small number of non-zeros, higher level caches can still hold the data for ELLPACK. Otherwise, the column-major storage of ELLPACK leads to higher cache-miss rates and more memory operations.

## OpenMP
\label{sec:omp_results}

Fig. \ref{fig:rate_coded_omp} depicts the achieved performance using OpenMP for CSR (orange), ELLPACK-R (blue) and CSB (red). **TODO: why not ELLPACK?** As for the single-threaded implementation, ELLPACK-R performs worse than CSR and CSB across nearly all investigated matrices. It is only comparable to CSR when the number of non-zeros is low.

CSB and ELLPACK-R achieve speedups in a range of 10 to 14 on most of the tested configurations. CSR reaches considerably high speedups on the FNP matrices but is impaired (speedups below 8) on the RF and dense matrices. Scalability breaks down for these matrices when there is a large number of non-zeros. CSB achieves high speedups on the RF and dense matrices, bringing the computation time close to CSR. CSB even outperforms CSR on FNP matrices, although this effect seems to decrease for larger numbers of connections.

As discussed in Section \ref{openmp_impl}, OpenMP implementations do not use explicit synchronization such as barriers or atomics. As SpMV is memory-bound, the reason for this behavior (**What behavior?**) is most likely the false sharing of memory resources. Another performance limiter could be the thread scheduling: the number of cache-misses or NUMA remote memory accesses is higher for CSR than for CSB and ELLPACK on the RF matrices. A detailed investigation of cache-miss statistics is out of the scope of this article but could be interesting for future work.

\begin{figure}[htbp]
    \includegraphics[width=\textwidth]{images/rate_code_psp_omp.png}
    \caption{Best achieved computation time and speedups using OpenMP (1 to 16 threads). The x-axis represents the test matrices and the y-axis A) the average computation time in milliseconds and B) the achieved speedup against the single-thread implementation. We consider three matrix formats: CSR (orange), ELLPACK (blue) and CSB (red). CSR and ELLPACK behave similarly, as in the single-thread case. The CSB performance gets closer to CSR and ELLPACK, while there are some configurations where CSB clearly outperform CSR and ELLPACK. This is explained by a better scalability of CSB in comparison to CSR and ELLPACK, especially for RF and Dense matrices. However, all implementations tend to saturate below the maximum achievable speedup of 16.}
    \label{fig:rate_coded_omp}
\end{figure}

## MPI
\label{sec:mpi_results}

Fig. \ref{fig:rate_coded_mpi} shows the achieved performance of the OpenMP (red), MPI (orange) and hybrid MPI/OpenMP (blue) implementations using the CSR format. Both MPI implementations outperform the OpenMP implementation, what highlights the issues of the OpenMP implementation with data locality and irregular accesses. For the pure and hybrid MPI implementations, socket local data locality is ensured and thread placement is fixed but the performance on RF matrices still breaks down. This limitation might be a result of the available memory bandwidth, as described by \cite{Kourtis2010}.

As described in section \ref{openmp_impl}, MPI implementations require to transfer at each time step the firing rate of the 40,000 neurons. The times presented in the figure comprise these communication times. We also measured them separately and noticed that the fraction consumed by this *MPI\_Allgather* operation, even for mid- and large-scale networks, is in the single-digit percentage range and are therefore not a major performance limiter. This could then explain why the pure and hybrid MPI implementations behave closely on our test set.

**TODO: explain why only CSR?**

**TODO: replace OpenMPI with MPI in the figure**.

\begin{figure}[htbp]
    \includegraphics[width=\textwidth]{images/rate_code_psp_mpi.png}
    \caption{MPI performance using the CSR format across multiple test matrices. The figure depicts the achieved performance of the pure MPI (orange), hybrid MPI (blue) and OpenMP (red, for comparison) implementations using the CSR format. Both MPI implementations perform better than the openMP implementation, likely because of the better data locality and thread placement allowed by MPI. As with openMP, the achieved performance on RF matrices is rather limited for both implementations.}
    \label{fig:rate_coded_mpi}
\end{figure}

## CUDA
\label{sec:cuda_results}

Fig. \ref{fig:rate_coded_cuda} depicts the achieved performance using CUDA for the CSR and ELLPACK-R formats (**TODO: why not CSB?**). Furthermore, we compare three variants of the CSR format, the scalar (red) and vector (blue) implementations proposed by \citep{Bell2009} and the vector and multiple warps (yellow) proposed by \citep{Dinkelbach2012}. The speedups for the CSR variants are computed with respect to the single-thread CSR times. The speedups reported for ELLPACK-R are against the ELLPACK CPU implementation, as the ELLPACK-R performs badly on CPUs.

We compare the achieved speedups on two cards, a Keplar K20m (Fig. \ref{fig:rate_coded_cuda}B, straight line) and a GTX 1080Ti (Fig. \ref{fig:rate_coded_cuda}C, dashed lines (**TODO: why dashed?**)). The scalar implementation (red) is intended for a small number of non-zeros per row and consequently performs well on FNP 16 or RF 5x5. This in in agreement with the results of \cite{Bell2009}, where this format was only performing well on the Epidemiology matrix, which had 4 non-zeros per row. The reason for this poor performance is a bad usage of coalesced memory accesses of the data arrays. The vector variant (blue) allows a better coalesced access to the arrays, what explains its better performance. Our proposed approach, the vector and multiple warps variant (yellow), performs best on the dense matrices and the sparse ones with a large number of non-zeros per row (FNP 2048, FNP 4096 and RF 40x40), showing the suitability of this implementation for high numbers of non-zeros per row. Nevertheless, the vector implementation of \cite{Bell2009} can be seen as the most flexible CSR implementation as it performs well on most of the tested configurations.


**I do not get the point of that comparison. Yellow and blue are not similar on the GTX either. Show only GTX results, the K20 are outdated.** Nonetheless, one can see that only on the Keplar K20m is a noticeable difference between the vector (blue) and the modified vector (yellow) version, whereas on the GTX1080Ti card both implementations behave similarly.

As expected from \cite{Vazquez2012}, the ELLPACK-R format (green) can outperform the CSR variants on the small- and mid-scale FNP and RF matrices. The computation time of the ELLPACK-R format on dense matrices is at the level of the scalar kernel, probably because of higher memory bandwidth requirements. The ELLPACK performance was omitted, as it is at the level of scalar CSR, mainly because of the same problems.


**TODO: replace ELLPACK-R in the figure. Order of the legend should be scalar, vector, vector+, ellpack-r**

\begin{figure}[htbp]
    \includegraphics[width=\textwidth]{images/multi_host_rate_code_psp_cuda.png}
    \caption{Best achieved computation time and speedups for the CUDA implementations. They are reported for three variants of CSR and an ELLPACK-R implementation. The scalar implementation (red) is intended for small numbers of non-zeros per row and consequently perform well on FNP 16 or RF 5x5. The vector and multiple warps variant (orange) performs best on the dense matrices, proving their suitability for a high number of non-zeros per row. The vector implementation (blue) performs well on most of the tested configurations. The ELLPACK-R format (green) works nicely for smaller matrices in the FNP test set and on RF but the performance breaks on dense matrices.}
    \label{fig:rate_coded_cuda}
\end{figure}


# Discussion
\label{sec:discussion}

**TODO:
Rewrite the discussion with a clear summary at the beginning. Discuss then each INTERESTING point in one paragraph. Perhaps split the discussion into subsections to group your points together**.

We have investigated the parallel performance of synaptic transmission in rate-coded neural networks on parallel shared-memory hardware. For this purpose, we compared different matrix formats (CSR, CSB, ELLPACK and ELLPACK-R) and implemented them on different parallel paradigms (openMP, MPI and CUDA).

**TODO: make a better summary of the main messages of your paper**
*We show that multi-core implementations should be preferred for small networks with sparse connectivity.
The performance of this operation performs well if caches on CPUs respectively coalescence on GPUs can effectively used.
Consequently, on both CPUs and GPUs the parallel evaluation of the synaptic transmission benefits from clustered connections.
As outlined in the introduction the choice of data structure is dependent on two factors: the connectivity and the target platform.*

There exists a large variety of test matrices in the SpMV literature, collected to a larger extent in the SuiteSparse Matrix Collection. These matrices are derived from several scientific applications, among others physic simulations or engineering. Many authors provide performance results on a subset of these matrices or provide new ones coming from their own work. However, the matrices differ often in terms of number per rows, (average) number of non-zeros and consequently the density. \cite{Sedaghati2015} compared for example 27 different matrices, where the largest matrix had a density of 0.3276% and only 5 matrices were above 0.1%.
However, the connectivity matrices used in large-scale rate-coded models can easily have higher densities (section \ref{sec:testset}), what motivated us to create a different test set for our comparisons in this article. The fixed probability (FP) pattern can be found in performance studies done for the simulation of spiking neural networks \citep[e.g.][]{Ippen2017, Jordan2018}.


<!-- In case of \cite{Vazquez2011} and \cite{Vazquez2012}, who used 21 test matrices, the most matrices had below 100 non-zeros per row. The density of the matrices was in most cases below 1%.
The work of \cite{Williams2009} used 14 matrices, also used by the work of \cite{Bell2009}, which exceed only in two cases 100 non-zeros per row.
Except for the dense matrix (2,000 non-zeros) the density is below 1%. \cite{Goumas2008} used 100 different matrices mostly selected from SuiteSparse Matrix collection with a larger variation in terms of matrix size and non-zeros, even though the density of the selected matrices is often below 1%.  -->


<!-- Contrary, to the previously described SpMV literature we did not report the achieved performance in floating operations per flops (FLOPs), rather we will compare the required computation time in milliseconds and speedups as done in computational neuroscience literature \citep[e.g.][]{Nowotny2011, Dinkelbach2012, VanAlbada2014, Yavuz2015}. -->

Direct comparisons between CPU and GPU implementations should be done carefully, as already discussed in \cite{Nowotny2011} and \cite{Dinkelbach2012}, because of the heterogeneous character of the hardware: CPUs and GPUs strongly differ in available memory speed, cache architecture and size, etc. In this article, we have compared all implementations with an optimized single thread implementation as the baseline. **Is there really a need for that discussion again?**

Our analysis compares three sparse matrix formats: compressed sparse row (CSR), compressed sparse blocks (CSB) and ELLPACK / ELLPACK-R. While the first is often used in neural simulation environments, we are not aware of implementations using the other two formats either on CPUs or GPUs. In the present article, we omit an object-oriented implementation, as it is done in NEST, for rate-coded synapses as we have done this in previous work and observed a performance limitation especially on GPUs due uncoalesced memory accesses \citep{Dinkelbach2012}.

Using OpenMP, CSR achieved the lowest computation time on most matrices although the achieved speedups were low. (**TODO: should go in the summary**)

The performance on NUMA systems seems to be impaired by false sharing, which can be ruled out by using a pure or hybrid MPI approach, as the neuro-simulators NEST and Auryn do. The performance improvement of the hybrid implementation is ensured by the NUMA awareness, as already shown by \cite{Kourtis2010}. Generally, the parallel performance stays below its potential maximum, as a result of the used memory bandwidth.
Further work could test if other formats like the CSR modifications proposed by \cite{Kourtis2010} could further improve the performance.

The comparison between the openMP and the MPI implementations show the importance of data locality and thread placement.
However, for large-scale rate-coded networks, the data distribution and node local allocation must be considered, as already demonstrated by NEST for the simulation of spiking  \citep{Ippen2017, Jordan2018} and rate-coded  \citep{Hahne2017} networks.

In our experiments, the CSB format was not able to outperform the CSR format in most cases. However, one should highlight the potential memory reduction by the CSB while maintaining a CSR comparable performance. Secondly, the CSB seems to have a better scalability behavior in comparison to CSR.

The CSR implementations on GPUs behave as expected from the literature. The scalar implementation \citep{Bell2009} performs well for small network configurations (FNP 16, FNP 32 and RF 5x5) but exhibit bad performance for bigger or denser matrices.
<!-- In the original paper of \cite{Bell2009} the scalar kernel was only effective on the Epidemiology matrix, which contained 4 non-zeros per row. -->
For larger network configurations, our modified vector version outperforms the vector version of \citep{Bell2009}, as the additional reduction stage is targeting a larger number of non-zeros per row.
Nevertheless, for most of our considered test matrices, the vector format proposed by \cite{Bell2009} format performs better than other CSR variants.

In line with previous publications \citep{Vazquez2011, Vazquez2012, Sedaghati2015}, the ELLPACK and ELLPACK-R are beneficial on GPUs and are able to outperform the CSR variants.
The work of \cite{Sedaghati2015} stated a better suitability of ELLPACK based on the density (**than what?**), even though their test matrices were in general sparser than ours.
The comparably high speedups of 20 up to 25 for the largest matrices in the test set should not be overrated considering the poor performance of the ELLPACK format on single-thread CPUs. (**Shouldn't that go in the results instead?**)

<!-- **Already said multiple times** The choice of a sufficient sparse matrix format can be highly dependent on the matrix characteristics, e. g. size of the matrix or number of non-zeros per row relative to the available caches \citep{James1997}. \cite{Goumas2008} investigated the performance of CSR and blocked compressed sparse row (BCSR) formats on 100 different matrices concluding with 6 optimization rules for SpMV implementations, especially focusing on working set size, matrix structure and access to the dense vector. They point out, that filling up with zeros, e. g. in the ELLPACK format or compression (e. g. \cite{Willcock2006} or \cite{Kourtis2010}) is not always effective as working set size is increased. The work of \cite{Sedaghati2015} indicates that the percentage of non-zeros in a matrix might be a good selection property, but they also demonstrated exceptions where this rule was misleading.  -->

As highlighted by \cite{Sedaghati2015} or \cite{Greathouse2014}, there seems not to be "one-size-fits-all" format. Based on the test matrices investigated in this article, we can conclude that the choice of the data structure should be primarily guided by the target platform and by the statistics of the matrix. While CSR is efficient for the simulation on multi-core CPUs across all tested configurations and CSB seems to be valuable due to memory reduction, the picture is more complicated on GPUs. **TODO: summarize that picture, extend** GPUs are more effective for a large number of non-zeros.

This work could be extended into multiple directions. We could first expand the variety of formats using an ELLPACK implementation as a basis, for example BELLPACK \citep{Choi2010}. This would go in line with work on auto-tuning related to the thread/block configuration, as discussed in particular by \cite{Filippone2017}. The second direction would be the improvement of the hybrid/pure MPI implementation and how these implementations behave in full network scenarios, for example when integrated in the neuro-simulator ANNarchy \citep{Vitay2015}. Third, one could investigate whether formats like CSB could be used for spiking neural networks in order to improve either performance or reduce the memory consumption of the connectivity matrices.
